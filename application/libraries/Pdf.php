<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

class Pdf {

    function createPDF($html, $filename = '', $download = TRUE, $paper = 'A4', $orientation = 'portrait') {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        $output = $dompdf->output();
//        $dompdf->stream($filename, array('Attachment' => 0));
        file_put_contents("./assets/file/invoice/".$filename, $output);
//        $pdfFilePath = FCPATH . "assets/file/invoice/" . $filename;
//        $dompdf = new Dompdf\DOMPDF();
//        $dompdf->load_html($html);
//        $dompdf->set_paper($paper, $orientation);
//        $dompdf->render();
//        $pdf_string = $dompdf->output();
//        file_put_contents($pdfFilePath, $pdf_string);
//        if($download)
//            $dompdf->stream($filename, array('Attachment' => 1));
//        else
//            $dompdf->stream($filename, array('Attachment' => 0));
    }

}

?>