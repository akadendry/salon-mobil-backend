<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Booking');
        if ($this->session->userdata('email') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $company_id = $this->session->userdata('company_id');
        $this->data['data_booking'] = $this->M_Booking->get_all_data($company_id)->result();
        //nebeng update data belum bayar
        $data = array(
            'status_bayar' => 2,
            'statusDp' => 2
        );
        $dataLewatBayar = $this->M_Booking->getAllDataBookingLewatBayar();
        foreach ($dataLewatBayar as $row):
            $id_book = $row->id_booking;
            $this->M_Booking->updateDataLewatBayar($data, $id_book);
        endforeach;

        $this->data['size'] = $this->M_Booking->getAllSize();
        $this->data['menuActiveParent'] = '';
        $this->data['menuActive'] = 'booking';
        $this->load->view('booking', $this->data);
    }

    public function approve() {
        $id_booking = $this->input->post('id_booking');
//        echo $id_service;
//        exit();
//        $id_booking = $this->uri->segment('3');
        $data = array(
            'status_bayar' => 1,
            'approved_by' => $this->session->userdata('id'),
            'statusDp' => 1
        );
        $updated = $this->M_Booking->updated($data, $id_booking);

        $dataPrecheck = array(
            'id_booking' => $id_booking,
            'type_booking' => 'online',
            'created_by' => $this->session->userdata('id'),
            'created_at' => date('Y-m-d H:i:s')
        );
        $insert = $this->M_Booking->insertPrecheck($dataPrecheck);

        $no_polisi = $this->M_Booking->getNoPolisiByIdBooking($id_booking);
        $noPolisi = str_replace(' ', '-', $no_polisi);
        if (!is_dir('./assets/images/precheck/' . $noPolisi)) {
            mkdir('./assets/images/precheck/' . $noPolisi, 0777, TRUE);
        }

        $customer_id = $this->M_Booking->getCustomerIdByIdBooking($id_booking);
        $data_customer = $this->M_Booking->getDataCustomerById($customer_id);
        foreach ($data_customer as $dc):
            $email_customer = $dc->email;
        endforeach;

        //cetak dan save pdfnya
        $filenameinvoice = strtotime(date('Y-m-d H:i:s')) . ".pdf";
        $this->cetakInoviceDpPdf($id_booking, $filenameinvoice);
        //akhir cetak dan save pdfnya
        //kirim email
        $config = Array(
            'smtp_host' => 'mail.detailingshopgarage.com',
            'smtp_port' => 587,
            'smtp_user' => 'no-reply@detailingshopgarage.com', // change it to yours
            'smtp_auth' => TRUE,
            'smtp_pass' => 'Password1', // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => TRUE
        );
        $pdfFilePath = FCPATH . "assets/file/invoice/" . $filenameinvoice;
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('admin@dsgindonesia.com', 'DSG Indonesia');
        $this->email->to($email_customer); // change it to yours
        $this->email->subject('Invoice Shop Garage');
        $this->data['recipient_email'] = $email_customer;
        $this->data['subjek_email'] = "Invoice";
        $email = $this->load->view('v_email_template_invoice', $this->data, TRUE);
        $this->email->message($email);
        $this->email->attach($pdfFilePath);
        $this->email->send();
        //akhir kirim email

        if ($updated == 1 && $insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Pembayaran berhasil diapprove');
            return redirect('booking');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan');
            return redirect('booking');
        }
    }

    public function getDataPriceByIdService($id) {
//        echo $id;
//        exit();
        $pemisah = explode("-", $id);
        $idSize = $pemisah[0];
        $idService = $pemisah[1];
        $tmp = '';
        $data = $this->M_Booking->getDataPriceByIdServices($idService, $idSize);
        if (!empty($data)):
            $tmp .= "Price : ";
            foreach ($data as $row):
                $hargaAwal = number_format($row->price);
                $tmp .= 'Rp.' . $hargaAwal;
            endforeach;
            $tmp .= "<br>Booking DP : Rp.500,000";
        else:
            $tmp .= "Data Harga Tidak Ditemukan Harap Hubungi Admin Kami";
        endif;
        die($tmp);
    }

    public function cetakInoviceDpPdf($idbooking, $filename) {

        $data['data'] = $this->M_Booking->get_data_transaction_by_id_booking($idbooking);
//        foreach ($data['data'] as $row):
//            //base 64 image
//            $path = base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_admin);
//            $type = pathinfo($path, PATHINFO_EXTENSION);
//            $datas = file_get_contents($path);
//            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($datas);
//            $data['data'][0]->sign_admin_64 = $base64;
//        endforeach;
        //cetak dan save pdfnya
        #--CREATE PDF
        $this->load->library('pdf');
        $data['tanggal'] = date('d M Y');
        $html = $this->load->view('pdf_invoiceDp', $data, true);
        $this->pdf->createPDF($html, $filename);

        //update invoice di db
        $dataBooking = array(
            'invoice_dp' => $filename
        );
        $this->M_Booking->updatedBooking($dataBooking, $idbooking);
        //akhir update data
    }

}
