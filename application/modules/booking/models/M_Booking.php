<?php

class M_Booking extends CI_Model {

    function get_all_data($company_id) {
        $this->db->select('booking_temp.*, mst_users.email, mst_users.name, mst_users.phone, mst_service.*, mst_brand.name as nama_merk, mst_brand_type.name as nama_tipe');
        $this->db->from('booking_temp');
        $this->db->join('mst_users', 'booking_temp.id_user = mst_users.id_user');
        $this->db->join('mst_service', 'booking_temp.name_service = mst_service.id_service');
        $this->db->join('mst_brand', 'booking_temp.car_brand = mst_brand.id');
        $this->db->join('mst_brand_type', 'booking_temp.car_type = mst_brand_type.id');
        $this->db->where('company_id', $company_id);
        $this->db->order_by("status_bayar", "asc");
        $this->db->order_by("id_booking", "desc");

        return $this->db->get();
    }

    public function insertPrecheck($data) {
        $query = $this->db->insert('precheck', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updated($data, $id_booking) {
        $this->db->where('id_booking', $id_booking);
        $query = $this->db->update('booking_temp', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateDataLewatBayar($data, $id_book) {
        $this->db->where('id_booking', $id_book);
        $query = $this->db->update('booking_temp', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
//    public function updateDataLewatBayar($data, $id_booking) {
//        $this->db->where('id_booking', $id_booking);
//        $query = $this->db->update('booking_temp', $data);
//        if ($query) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }

    public function getNoPolisiByIdBooking($id_booking) {
        $data = $this->db->query("SELECT * FROM booking_temp WHERE id_booking = '$id_booking'");
        $rsl = $data->result();
        foreach ($rsl as $row) {
            $no_polisi = $row->no_polisi;
        }
        return $no_polisi;
    }
    
    public function getCustomerIdByIdBooking($id_booking) {
        $data = $this->db->query("SELECT * FROM booking_temp WHERE id_booking = '$id_booking'");
        $rsl = $data->result();
        foreach ($rsl as $row) {
            $id_user = $row->id_user;
        }
        return $id_user;
    }
    
    public function getDataCustomerById($customer_id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_users 
                                  WHERe id_user = $customer_id
                                  ORDER BY id_user ASC");
        return $data->result();
    }
    
    public function getAllDataBookingLewatBayar() {
        $data = $this->db->query("SELECT * FROM booking_temp WHERE type_booking = 'online' AND statusDp = 0 AND CURRENT_TIMESTAMP >= expireDateDp");
        return $data->result();
    }
    
    public function getAllSize() {
        $data = $this->db->query("SELECT * FROM car_size ORDER BY id_size ASC");
        return $data->result();
    }
    
    public function getDataPriceByIdServices($idService, $idSize) {
        $data = $this->db->query("SELECT *
                FROM mst_price
                WHERE is_active = 1 AND id_service = '$idService' AND id_size = '$idSize'");
        return $data->result();
    }
    
    public function get_data_transaction_by_id_booking($id_booking){
        $data = $this->db->query("SELECT a.*,
                b.name_service as nama_servis, c.name as nama_brand, 
                d.name as nama_tipe, e.name_size as ukuran, f.name as nama_customer, g.sign_admin
                FROM  booking_temp a 
                LEFT JOIN mst_service b ON a.name_service = b.id_service
                LEFT JOIN mst_brand c ON a.car_brand = c.id
                LEFT JOIN mst_brand_type d ON a.car_type = d.id
                LEFT JOIN car_size e ON a.id_size = e.id_size
                LEFT JOIN mst_users f ON a.id_user = f.id_user
                LEFT JOIN precheck g ON a.id_booking = g.id_booking
                WHERE a.id_booking = $id_booking");
        return $data->result();
    }
    
    public function updatedBooking($dataBooking, $idBooking) {
        $this->db->where('id_booking', $idBooking);
        $query = $this->db->update('booking_temp', $dataBooking);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

}
