<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Branch');
        if ($this->session->userdata('email') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function transaction() {
        $company_id = $this->session->userdata('company_id');
        $this->data['branch'] = $this->M_Branch->getAllData($company_id);
        $this->data['menuActiveParent'] = 'branch';
        $this->data['menuActive'] = 'branch_transaksi';
        $this->data['cari'] = 0;
        $this->data['transaksi'] = array();
        $this->data['total_bayar_pusat'] = 0;
        $this->load->view('transaction', $this->data);
    }
    
    public function searchDataTransaksi(){
        $company_id_pusat = $this->session->userdata('company_id');
        $company_id = $this->input->post('branch');
        $start_date = $this->input->post('start_date');
        if($start_date != ''){
            $start_date = date('Y-m-d', strtotime($start_date));
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date('Y-m-d', strtotime($end_date));
        }
        $this->data['transaksi'] = $this->M_Branch->getDataTransaksiByCompany($company_id, $start_date,$end_date);
        $this->data['total_bayar_pusat'] = 0;
        $get_sum = $this->M_Branch->getBayarKePusat($company_id, $start_date,$end_date);
        foreach($get_sum as $row):
            $this->data['total_bayar_pusat'] = $row->harga_total;
        endforeach;
        $this->data['cari'] = 1;
        $this->data['menuActiveParent'] = 'branch';
        $this->data['menuActive'] = 'branch_transaksi';
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        $this->data['branch_id'] = $company_id;
        $this->data['branch'] = $this->M_Branch->getAllData($company_id_pusat);
        $this->load->view('transaction', $this->data);
    }
    
    public function transactionExportExcel(){
        $company_id = $this->input->post('branch_id');
        $start_date = $this->input->post('start_date');
        if($start_date != ''){
            $start_date = date('Y-m-d', strtotime($start_date));
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date('Y-m-d', strtotime($end_date));
        }
        $this->data['transaksi'] = $this->M_Branch->getDataTransaksiByCompany($company_id, $start_date,$end_date);
        $this->data['total_bayar_pusat'] = 0;
        $get_sum = $this->M_Branch->getBayarKePusat($company_id, $start_date,$end_date);
        foreach($get_sum as $row):
            $this->data['total_bayar_pusat'] = $row->harga_total;
        endforeach;
        $get_data_branch = $this->M_Branch->getDataBranchById($company_id);
        if(count($get_data_branch) > 0):
            foreach($get_data_branch as $gdb):
                $nama_cabang = $gdb->name;
            endforeach;
        else:
            $nama_cabang = '';
        endif;
        if($nama_cabang == ''):
            $this->data['title'] = "Report Transaksi Cabang";
        else:
            $this->data['title'] = "Report Transaksi ".$nama_cabang;
        endif;
        $this->load->view('cetak_transaction', $this->data);
    }
}