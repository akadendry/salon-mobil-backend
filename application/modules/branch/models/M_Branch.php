<?php

class M_Branch extends CI_Model {

    public function getAllData($company_id_pusat) {
        $data = $this->db->query("SELECT 
                                    *
                                FROM 
                                    mst_company 
                                WHERE 
                                    deleted_at is null
                                    AND
                                    id != $company_id_pusat");
        return $data->result();
    }
    
    public function getDataBranchById($branch_id) {
        $data = $this->db->query("SELECT 
                                    *
                                FROM 
                                    mst_company 
                                WHERE 
                                    deleted_at is null
                                    AND
                                    id = $branch_id");
        return $data->result();
    }
    
    public function getDataTransaksiByCompany($company_id, $start_date,$end_date){
        if($start_date == '' && $end_date == ''){
            $kondisi = '';
        }
        if($start_date != '' && $end_date == ''){
            $kondisi = "AND DATE(a.book_date) >= '$start_date'";
        }
        if($start_date == '' && $end_date != ''){
            $kondisi = "AND DATE(a.book_date) <= '$end_date'";
        }
        if($start_date != '' && $end_date != ''){
            $kondisi = "AND (DATE(a.book_date) BETWEEN '$start_date' AND '$end_date')";
        }
        $data = $this->db->query("SELECT 
                                    a.book_date as tanggal_transaksi, a.name_service as id_service, a.id_price, a.price, b.margin, c.name_service as nama_service, d.name as nama_pelanggan
                                FROM 
                                    booking_temp a
                                LEFT JOIN service_margin_price b ON a.name_service = b.service_id AND a.company_id = b.company_id
                                LEFT JOIN mst_service c ON a.name_service = c.id_service
                                LEFT JOIN mst_users d ON a.id_user = d.id_user
                                WHERE 
                                        a.company_id = $company_id
                                            $kondisi
                                    AND
                                        a.deleted_at is null");
//        echo $this->db->last_query();
//        exit();
        return $data->result();
    }
    
    public function getBayarKePusat($company_id, $start_date,$end_date){
        if($start_date == '' && $end_date == ''){
            $kondisi = '';
        }
        if($start_date != '' && $end_date == ''){
            $kondisi = "AND DATE(a.book_date) >= '$start_date'";
        }
        if($start_date == '' && $end_date != ''){
            $kondisi = "AND DATE(a.book_date) <= '$end_date'";
        }
        if($start_date != '' && $end_date != ''){
            $kondisi = "AND (DATE(a.book_date) BETWEEN '$start_date' AND '$end_date')";
        }
        $data = $this->db->query("SELECT 
                                    SUM(b.margin) AS harga_total
                                FROM 
                                    booking_temp a
                                LEFT JOIN service_margin_price b ON a.name_service = b.service_id AND a.company_id = b.company_id
                                LEFT JOIN mst_service c ON a.name_service = c.id_service
                                LEFT JOIN mst_users d ON a.id_user = d.id_user
                                WHERE 
                                        a.company_id = $company_id
                                            $kondisi
                                    AND
                                        a.deleted_at is null");
        return $data->result();
    }

}
