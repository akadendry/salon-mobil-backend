<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Transaksi Cabang</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>

                        <li>
                            <a href="#">Data Transaksi Cabang</a>
                        </li>
                    </ul><!-- /.breadcrumb -->
                </div>

                <div class="page-content">

                    <div class="page-header">
                        <h1>
                            Data Transaksi Cabang
                        </h1>
                    </div><!-- /.page-header -->
                    <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3): ?>
                                    <?php if ($cari == 0): ?>
                                        <div class="col-xs-12">
                                            <div style="float: right;">
                                                <a class="btn btn-lg btn-round btn-warning" id="searchButton" onchange="formFilter(this)">
                                                    <i class="fa fa-search"></i> Search
                                                </a>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <?php
                                            $branch_id_excel = $branch_id;
                                            if($start_date == ''){
                                                $start_date_excel = '';
                                            }else{
                                                $start_date_excel = date('Y-m-d', strtotime($start_date));
                                            }
                                            if($end_date == ''){
                                                $end_date_excel = '';
                                            }else{
                                                $end_date_excel = date('Y-m-d', strtotime($end_date));
                                            }
                                        ?>
                                        <div class="col-xs-12">
                                            <div style="float: right;">
                                                <form class="form-group" action="<?php echo base_url('branch/transactionExportExcel') ?>" method="post">
                                                    <input type="hidden" name="start_date" value="<?= $start_date_excel?>"/>
                                                    <input type="hidden" name="end_date" value="<?= $end_date_excel?>"/>
                                                    <input type="hidden" name="branch_id" value="<?= $branch_id_excel?>"/>
                                                    <button type="submit" class="btn btn-lg btn-round btn-success" id="cetakExcel">
                                                        <i class="fa fa-print"></i> Cetak
                                                    </button>
                                                    <a class="btn btn-lg btn-round btn-warning" id="searchButton" onchange="formFilter(this)">
                                                        <i class="fa fa-search"></i> Search
                                                    </a>
                                                </form>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12" style="margin-left: 13px;">
                                        <?php if ($cari == 0): ?>
                                            <?php
                                            $start_date = '';
                                            $end_date = '';
                                            $branch_id = '';
                                            ?>
                                            <div class="formSearch" style="display:none;">
                                            <?php else: ?>
                                                <?php
                                                $start_date = $start_date;
                                                $end_date = $end_date;
                                                $branch_id = $branch_id;
                                                ?>
                                                <div class="formSearch" style="display:block;">
                                                <?php endif; ?>
                                                <form class="form-group filter" action="<?php echo base_url('branch/searchDataTransaksi') ?>" method="post">
                                                    <div class="row" style="width: 100%;">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">          
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <label for="tipe" class="control-label">Branch</label>
                                                                    <select class="form-control select2" name="branch" required style="text-align: center;">
                                                                        <option value="" <?php if($branch_id == ''){echo 'selected';}?>>--Pilih Branch--</option>
                                                                        <?php foreach($branch as $br):?>
                                                                            <option value="<?=$br->id?>" <?php if($branch_id == $br->id){echo 'selected';}?>><?= $br->name?></option> 
                                                                        <?php endforeach;?>                         
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-top: 30px;">          
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <label for="tipe" class="control-label">Tanggal Mulai</label>
                                                                    <div class="input-group date date_start">
                                                                        <input type="text" name="start_date" id="start_date" value="<?= $start_date ?>" class="form-control" />
                                                                        <span class="input-group-addon">
                                                                            <i class="ace-icon fa fa-calendar"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                    <label for="tipe" class="control-label">Tanggal Akhir</label>
                                                                    <div class="input-group date date_end">
                                                                        <input type="text" name="end_date" id="end_date" value="<?= $end_date ?>" class="form-control" />
                                                                        <span class="input-group-addon">
                                                                            <i class="ace-icon fa fa-calendar"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>     
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                    <label for="tipe" class="control-label">&nbsp;</label>
                                                                    <div class="m-t-3">
                                                                        <input type="submit" class="btn btn-primary" value="cari" name="cari" style="width: 100%;">
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">&nbsp;</div>
                                    <div class="col-xs-12">
                                        <div class="table-header">
                                            &nbsp;
                                        </div>

                                        <!-- div.table-responsive -->

                                        <!-- div.dataTables_borderWrap -->

                                        <div style="overflow-x:auto;">
                                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Customer</th>
                                                        <th>Nama Service</th>
                                                        <th>Tanggal Service</th>
                                                        <th>Harga</th>
                                                        <th>Margin</th>
                                                        <th hidden>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $no = 1; 
                                                        $total_hargas = 0;
                                                        $total_price = 0;
                                                    ?>
                                                    <?php if(count($transaksi > 0)):?>
                                                        <?php foreach ($transaksi as $row):?>
                                                            <tr>
                                                                <td> <?= $no++ ?> </td>
                                                                <td> <?= $row->nama_pelanggan ?></td>
                                                                <td> <?= $row->nama_service ?></td>
                                                                <td> <?= date('d-m-Y', strtotime($row->tanggal_transaksi)) ?></td>
                                                                <td> Rp. <?= number_format($row->price) ?></td>
                                                                <?php
                                                                    $sql ="SELECT * FROM mst_price WHERE id_price = ".$row->id_price;
                                                                    $query = $this->db->query($sql);
                                                                    if ($query->num_rows() > 0) {
                                                                      foreach ($query->result() as $rows) {
                                                                          $harga_basic = $rows->price;
                                                                      }
                                                                    }else{
                                                                        $harga_basic = 0;
                                                                    }
                                                                    $total_harga = (int) $row->price - (int) $harga_basic;
                                                                ?>
                                                                <td> Rp. <?= number_format($total_harga) ?></td>
                                                                <td hidden> &nbsp;</td>
                                                            </tr>
                                                            <?php $total_hargas += $total_harga; ?>
                                                            <?php $total_price += $row->price; ?>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4"><b>Total Pendapatan</b></td>
                                                        <td colspan="2"><b> Rp. <?= number_format($total_price) ?></b></td>
                                                        <td hidden>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5"><b>Total yang Harus Dibayar ke Pusat</b></td>
                                                        <td><b> Rp. <?= number_format($total_hargas) ?></b></td>
                                                        <td hidden>&nbsp;</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- PAGE CONTENT ENDS -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.page-content -->
                    </div>
                </div><!-- /.main-content -->


                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
                </a>
            </div><!-- /.main-container -->

            <!-- basic scripts -->

            <!--[if !IE]> -->
            <script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

            <!-- <![endif]-->

            <!--[if IE]>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->
            <script type="text/javascript">
                                            if ('ontouchstart' in document.documentElement)
                                                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            </script>
            <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

            <!-- page specific plugin scripts -->
            <script src="<?= base_url('assets/js/jquery-ui.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/dataTables.buttons.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/buttons.flash.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/buttons.html5.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/buttons.print.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/buttons.colVis.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/dataTables.select.min.js') ?>"></script>

            <!-- ace scripts -->
            <script src="<?= base_url('assets/js/ace-elements.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/ace.min.js') ?>"></script>

            <!-- inline scripts related to this page -->
            <script>
                $(document).ready(function () {
                    $('#searchButton').click(function () {
                        $('.formSearch').slideToggle("slow");
                    });
                });
            </script>
            <script type="text/javascript">
                jQuery(function ($) {
                    //initiate dataTables plugin
                    var myTable =
                            $('#dynamic-table')
                            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                            .DataTable({
                                bAutoWidth: false,
                                "aoColumns": [
                                    {"bSortable": false},
                                    null, null, null, null, null,
                                    {"bSortable": false}
                                ],
                                "aaSorting": [],

                                //"bProcessing": true,
                                //"bServerSide": true,
                                //"sAjaxSource": "http://127.0.0.1/table.php"	,

                                //,
                                //"sScrollY": "200px",
                                //"bPaginate": false,

                                //"sScrollX": "100%",
                                //"sScrollXInner": "120%",
                                //"bScrollCollapse": true,
                                //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                                //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                                //"iDisplayLength": 50


                                select: {
                                    style: 'multi'
                                }
                            });



                    $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                    new $.fn.dataTable.Buttons(myTable, {
                        buttons: [
                            {
                                "extend": "colvis",
                                "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                                "className": "btn btn-white btn-primary btn-bold",
                                columns: ':not(:first):not(:last)'
                            },
                            {
                                "extend": "copy",
                                "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "csv",
                                "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "excel",
                                "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "pdf",
                                "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                                "className": "btn btn-white btn-primary btn-bold"
                            },
                            {
                                "extend": "print",
                                "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                                "className": "btn btn-white btn-primary btn-bold",
                                autoPrint: false,
                                message: 'This print was produced using the Print button for DataTables'
                            }
                        ]
                    });
                    myTable.buttons().container().appendTo($('.tableTools-container'));

                    //style the message box
                    var defaultCopyAction = myTable.button(1).action();
                    myTable.button(1).action(function (e, dt, button, config) {
                        defaultCopyAction(e, dt, button, config);
                        $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                    });


                    var defaultColvisAction = myTable.button(0).action();
                    myTable.button(0).action(function (e, dt, button, config) {

                        defaultColvisAction(e, dt, button, config);


                        if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                            $('.dt-button-collection')
                                    .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                    .find('a').attr('href', '#').wrap("<li />")
                        }
                        $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                    });

                    ////

                    setTimeout(function () {
                        $($('.tableTools-container')).find('a.dt-button').each(function () {
                            var div = $(this).find(' > div').first();
                            if (div.length == 1)
                                div.tooltip({container: 'body', title: div.parent().text()});
                            else
                                $(this).tooltip({container: 'body', title: $(this).text()});
                        });
                    }, 500);





                    myTable.on('select', function (e, dt, type, index) {
                        if (type === 'row') {
                            $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
                        }
                    });
                    myTable.on('deselect', function (e, dt, type, index) {
                        if (type === 'row') {
                            $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
                        }
                    });




                    /////////////////////////////////
                    //table checkboxes
                    $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

                    //select/deselect all rows according to table header checkbox
                    $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
                        var th_checked = this.checked;//checkbox inside "TH" table header

                        $('#dynamic-table').find('tbody > tr').each(function () {
                            var row = this;
                            if (th_checked)
                                myTable.row(row).select();
                            else
                                myTable.row(row).deselect();
                        });
                    });

                    //select/deselect a row when the checkbox is checked/unchecked
                    $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
                        var row = $(this).closest('tr').get(0);
                        if (this.checked)
                            myTable.row(row).deselect();
                        else
                            myTable.row(row).select();
                    });



                    $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                        e.stopImmediatePropagation();
                        e.stopPropagation();
                        e.preventDefault();
                    });



                    //And for the first simple table, which doesn't have TableTools or dataTables
                    //select/deselect all rows according to table header checkbox
                    var active_class = 'active';
                    $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
                        var th_checked = this.checked;//checkbox inside "TH" table header

                        $(this).closest('table').find('tbody > tr').each(function () {
                            var row = this;
                            if (th_checked)
                                $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
                            else
                                $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
                        });
                    });

                    //select/deselect a row when the checkbox is checked/unchecked
                    $('#simple-table').on('click', 'td input[type=checkbox]', function () {
                        var $row = $(this).closest('tr');
                        if ($row.is('.detail-row '))
                            return;
                        if (this.checked)
                            $row.addClass(active_class);
                        else
                            $row.removeClass(active_class);
                    });



                    /********************************/
                    //add tooltip for small view action buttons in dropdown menu
                    $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

                    //tooltip placement on right or left
                    function tooltip_placement(context, source) {
                        var $source = $(source);
                        var $parent = $source.closest('table')
                        var off1 = $parent.offset();
                        var w1 = $parent.width();

                        var off2 = $source.offset();
                        //var w2 = $source.width();

                        if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                            return 'right';
                        return 'left';
                    }




                    /***************/
                    $('.show-details-btn').on('click', function (e) {
                        e.preventDefault();
                        $(this).closest('tr').next().toggleClass('open');
                        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                    });
                    /***************/





                    /**
                     //add horizontal scrollbars to a simple table
                     $('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
                     {
                     horizontal: true,
                     styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
                     size: 2000,
                     mouseWheelLock: true
                     }
                     ).css('padding-top', '12px');
                     */


                })
            </script>
            <script type="text/javascript">
                jQuery(function ($) {

                    $("#start_date").datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: false,
                        dateFormat: 'd-m-yy'
                    });
                    
                    $("#end_date").datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: false,
                        dateFormat: 'd-m-yy'
                    });
                });
            </script>
    </body>
</html>
