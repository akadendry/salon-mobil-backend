<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Datamaster extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Datamaster');
        if ($this->session->userdata('id') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function company() {
        $this->data['data_company'] = $this->M_Datamaster->getAllDataCompany();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_company';
        $this->load->view('list_company', $this->data);
    }

    public function addCompany() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_company';
        $this->load->view('add_company', $this->data);
    }

    public function insertCompany() {
        $this->db->trans_begin();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_company';
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'instagram' => $this->input->post('instagram'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone')
        );
        $company_id = $this->M_Datamaster->insertCompany($data);
        
        if($this->db->trans_status() === TRUE && ($company_id != null || $company_id != '')){
            $get_all_menu = $this->M_Datamaster->getAllMenu();
            foreach ($get_all_menu as $gam):
                $data1 = array(
                    'company_id' => $company_id,
                    'menu_id' => $gam->id,
                    'access' => 0
                );
                $insert_menu_access = $this->M_Datamaster->insertMenuAccess($data1);
            endforeach;
        }else{
            $this->db->trans_rollback();
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/addCompany');
        }

        if ($this->db->trans_status() === TRUE  && $insert_menu_access == 1) {
            $this->db->trans_commit();
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Company berhasil ditambahkan');
            return redirect('datamaster/editCompany/' . $company_id);
        } else {
            $this->db->trans_rollback();
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/addCompany');
        }
    }

    public function editCompany($id) {
        $this->data['company'] = $this->M_Datamaster->getDataCompanyById($id);
        $this->data['user'] = $this->M_Datamaster->getUserCmsByCompany($id);
        $this->data['margin'] = $this->M_Datamaster->getMarginByCompany($id);
        $data_access = array();
        $parentMenuAccess = $this->M_Datamaster->getParrentMenuAccessByCompany($id);
        foreach($parentMenuAccess as $row):
            $data_menu = array(
                'id' => $row->id,
                'company_id' => $row->company_id,
                'menu_id' => $row->menu_id,
                'access' => $row->access,
                'name' => $row->name,
                'icon' => $row->icon,
                'url' => $row->url,
                'order' => $row->order
            );
            $menu_id = $row->menu_id;
            $childMenuAccess = $this->M_Datamaster->getChildMenuAccessByCompany($id, $menu_id);
            if(count($childMenuAccess) > 0 ):
                $data_menu['child'] = $childMenuAccess;
            else:
                $data_menu['child'] = array();
            endif;
            array_push($data_access, $data_menu);
        endforeach;
        $this->data['data_menu_access'] = $data_access;
//        print_r($data_access);
//        exit();
        $this->data['company_id'] = $id;
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_company';
        $this->load->view('edit_company', $this->data);
    }

    public function updateCompany() {
        $this->db->trans_begin();
        $id = $this->input->post('id');
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('email'),
            'instagram' => $this->input->post('instagram'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone')
        );
        $update = $this->M_Datamaster->updateCompany($data, $id);
        
        if($this->db->trans_status() === TRUE && $update == 1){
            foreach ($this->input->post('menu') as $key=>$val):
                if($val == 'on'):
                    $val = 1;
                endif;
                $data_menu = array(
                    'access' => $val
                );
                $update_menu = $this->M_Datamaster->updateCompanyMenu($data_menu, $id, $key);
            endforeach;
        }else{
            $this->db->trans_rollback();
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/company');
        }
        
        if ($this->db->trans_status() === TRUE && $update_menu == 1) {
            $this->db->trans_commit();
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Brand berhasil Diubah');
            return redirect('datamaster/company');
        } else {
            $this->db->trans_rollback();
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/company');
        }
    }

    public function deleteCompany($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateCompany($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Company berhasil Dihapus');
            return redirect('datamaster/company');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/company');
        }
    }

    public function user() {
        $company_id = $this->session->userdata('company_id');
        $this->data['dat_user'] = $this->M_Datamaster->getAllDataUserCmsByBranch($company_id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_user';
        $this->load->view('list_user', $this->data);
    }

    public function addUser($company_id) {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_user';
        $this->data['company_id'] = $company_id;
        $this->data['role'] = $this->M_Datamaster->getAllDataRole();
        $this->load->view('add_user', $this->data);
    }

    public function insertUser() {
        $company_id = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_user';
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => sha1(md5($this->input->post('password'))),
            'role' => $this->input->post('role'),
            'verified' => 1,
            'company_id' => $this->input->post('company_id')
        );
        $insert = $this->M_Datamaster->insertUserCms($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data User berhasil ditambahkan');
            if ($company_id == 1) {
                return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
            } else {
                return redirect('datamaster/user');
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            if ($company_id == 1) {
                return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
            } else {
                return redirect('datamaster/user');
            }
        }
    }

    public function editUser($id, $company_id) {
        $this->data['user'] = $this->M_Datamaster->getUserCmsById($id);
        $this->data['role'] = $this->M_Datamaster->getAllDataRole();
        $this->data['company_id'] = $company_id;
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_user';
        $this->load->view('edit_user', $this->data);
    }

    public function updateUser() {
        $id = $this->input->post('id');
        $company_id = $this->input->post('company_id');
        if ($this->input->post('password') == null || $this->input->post('password') == '') {
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'role' => $this->input->post('role')
            );
        } else {
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'password' => sha1(md5($this->input->post('password'))),
                'role' => $this->input->post('role')
            );
        }

        $update = $this->M_Datamaster->updateUser($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data User berhasil Diubah');
            if ($this->session->userdata('company_id') == 1) {
                return redirect('datamaster/editCompany/' . $company_id);
            } else {
                return redirect('datamaster/user');
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            if ($this->session->userdata('company_id') == 1) {
                return redirect('datamaster/editCompany/' . $company_id);
            } else {
                return redirect('datamaster/user');
            }
        }
    }

    public function deleteUser($id, $company_id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateUser($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data User berhasil Dihapus');
            if ($this->session->userdata('company_id') == 1) {
                return redirect('datamaster/editCompany/' . $company_id);
            } else {
                return redirect('datamaster/user');
            }
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            if ($this->session->userdata('company_id') == 1) {
                return redirect('datamaster/editCompany/' . $company_id);
            } else {
                return redirect('datamaster/user');
            }
        }
    }

    public function customer() {
        $this->data['data_customer'] = $this->M_Datamaster->getAllDataCustomer();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_customer';
        $this->load->view('list_customer', $this->data);
    }

    public function brand() {
        $this->data['data_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_brand';
        $this->load->view('list_brand', $this->data);
    }

    public function addBrand() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_brand';
        $this->load->view('add_brand', $this->data);
    }

    public function insertBrand() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_brand';
        $data = array(
            'name' => $this->input->post('nama_brand'),
            'is_active' => 1,
        );
        $insert = $this->M_Datamaster->insertBrand($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Brand berhasil ditambahkan');
            return redirect('datamaster/brand');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/brand');
        }
    }

    public function editBrand($id) {
        $this->data['data_brand'] = $this->M_Datamaster->getBrandById($id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_brand';
        $this->load->view('edit_brand', $this->data);
    }

    public function updateBrand() {
        $id = $this->input->post('brand_id');
        $data = array(
            'name' => $this->input->post('nama_brand'),
        );
        $update = $this->M_Datamaster->updateBrand($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Brand berhasil Diubah');
            return redirect('datamaster/brand');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/brand');
        }
    }

    public function deleteBrand($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateBrand($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Brand berhasil Dihapus');
            return redirect('datamaster/brand');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/brand');
        }
    }

    public function type() {
        $this->data['data_type'] = $this->M_Datamaster->getAllDataType();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_type';
        $this->load->view('list_type', $this->data);
    }

    public function addType() {
        $this->data['data_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['data_size'] = $this->M_Datamaster->getAllSize();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_type';
        $this->load->view('add_type', $this->data);
    }

    public function insertType() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_type';
        $data = array(
            'brand_id' => $this->input->post('brand'),
            'size_id' => $this->input->post('size'),
            'name' => $this->input->post('type'),
            'is_active' => 1,
        );
        $insert = $this->M_Datamaster->insertType($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Type berhasil ditambahkan');
            return redirect('datamaster/type');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/type');
        }
    }

    public function editType($id) {
        $this->data['data_type'] = $this->M_Datamaster->getTypeById($id);
        $this->data['data_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['data_size'] = $this->M_Datamaster->getAllSize();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_type';
        $this->load->view('edit_type', $this->data);
    }

    public function updateType() {
        $id = $this->input->post('id');
        $data = array(
            'brand_id' => $this->input->post('brand'),
            'size_id' => $this->input->post('size'),
            'name' => $this->input->post('type')
        );
        $update = $this->M_Datamaster->updateType($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Type berhasil Diubah');
            return redirect('datamaster/type');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/type');
        }
    }

    public function deleteType($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateType($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Type berhasil Dihapus');
            return redirect('datamaster/type');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/type');
        }
    }

    public function service() {
        $company_id = $this->session->userdata('company_id');
        if ($company_id == 1) {
            $this->data['data_service'] = $this->M_Datamaster->getAllDataService();
        } else {
            $this->data['data_service'] = $this->M_Datamaster->getAllDataServiceMerge($company_id);
        }
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_service';
        $this->load->view('list_service', $this->data);
    }

    public function addService() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_service';
        $this->load->view('add_service', $this->data);
    }

    public function insertService() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_service';
        $data = array(
            'name_service' => $this->input->post('name_service'),
            'service_desc' => $this->input->post('service_desc'),
            'scope_service' => $this->input->post('scope_service'),
            'duration_time' => $this->input->post('duration_time'),
            'duration_day' => $this->input->post('duration_day'),
            'is_active' => 1,
        );
        $insert = $this->M_Datamaster->insertService($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Service berhasil ditambahkan');
            return redirect('datamaster/service');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/service');
        }
    }

    public function editService($id) {
        $this->data['data_service'] = $this->M_Datamaster->getServiceById($id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_service';
        $this->load->view('edit_service', $this->data);
    }

    public function updateService() {
        $id = $this->input->post('id_service');
        $data = array(
            'name_service' => $this->input->post('name_service'),
            'service_desc' => $this->input->post('service_desc'),
            'scope_service' => $this->input->post('scope_service'),
            'duration_time' => $this->input->post('duration_time'),
            'duration_day' => $this->input->post('duration_day')
        );
        $update = $this->M_Datamaster->updateService($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Service berhasil Diubah');
            return redirect('datamaster/service');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/service');
        }
    }

    public function deleteService($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateService($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Service berhasil Dihapus');
            return redirect('datamaster/service');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/service');
        }
    }

    public function price() {
        $company_id = $this->session->userdata('company_id');
        if ($company_id == 1) {
            $this->data['data_price'] = $this->M_Datamaster->getAllDataPrice();
        } else {
            $this->data['data_price'] = $this->M_Datamaster->getAllDataPriceMargin($company_id);
        }
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_price';
        $this->load->view('list_price', $this->data);
    }

    public function addPrice() {
        $this->data['data_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['data_size'] = $this->M_Datamaster->getAllSize();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_price';
        $this->load->view('add_price', $this->data);
    }

    public function insertPrice() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_price';
        $price = str_replace('Rp.', '', $this->input->post('price'));
        $price = str_replace('.', '', $price);
        $price = str_replace(',', '.', $price);
        $data = array(
            'id_service' => $this->input->post('id_service'),
            'id_size' => $this->input->post('id_size'),
            'price' => $price,
//            'downpayment' => $this->input->post('downpayment'),
            'is_active' => 1,
        );
        $insert = $this->M_Datamaster->insertPrice($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Price berhasil ditambahkan');
            return redirect('datamaster/price');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/price');
        }
    }

    public function editPrice($id) {
        $this->data['data_price'] = $this->M_Datamaster->getPriceById($id);
        $this->data['data_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['data_size'] = $this->M_Datamaster->getAllDataSize();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_price';
        $this->load->view('edit_price', $this->data);
    }

    public function updatePrice() {
        $id = $this->input->post('id_price');
        $price = str_replace('Rp.', '', $this->input->post('price'));
        $price = str_replace('.', '', $price);
        $price = str_replace(',', '.', $price);
        $data = array(
            'id_service' => $this->input->post('id_service'),
            'id_size' => $this->input->post('id_size'),
            'price' => $price,
//            'downpayment' => $this->input->post('downpayment')
        );
        $update = $this->M_Datamaster->updatePrice($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Price berhasil Diubah');
            return redirect('datamaster/price');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/price');
        }
    }

    public function deletePrice($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updatePrice($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Price berhasil Dihapus');
            return redirect('datamaster/price');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/price');
        }
    }

    //size
    public function size() {
        $this->data['data_size'] = $this->M_Datamaster->getAllDataSize();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_size';
        $this->load->view('list_size', $this->data);
    }

    public function addSize() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_size';
        $this->load->view('add_size', $this->data);
    }

    public function insertSize() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_size';
        $data = array(
            'name_size' => $this->input->post('name')
        );
        $insert = $this->M_Datamaster->insertSize($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Size berhasil ditambahkan');
            return redirect('datamaster/size');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/size');
        }
    }

    public function editSize($id) {
        $this->data['data_size'] = $this->M_Datamaster->getSizeById($id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_size';
        $this->load->view('edit_size', $this->data);
    }

    public function updateSize() {
        $id = $this->input->post('id_size');
        $data = array(
            'name_size' => $this->input->post('name_size'),
        );
        $update = $this->M_Datamaster->updateSize($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data size berhasil Diubah');
            return redirect('datamaster/size');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/size');
        }
    }

    public function deleteSize($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateSize($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Size berhasil Dihapus');
            return redirect('datamaster/size');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/size');
        }
    }

    public function getTypeCarByBrand() {
        $brand_id = $_POST['merk_id'];
        $option = '<option value="">-- Pilih Jenis Mobil --</option>';
        $get_type_car_by_brand = $this->M_Datamaster->getTypeCarByBrand($brand_id);
        foreach ($get_type_car_by_brand as $row):
            $option .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        endforeach;
        echo $option;
    }

    public function getSizeByType() {
        $type_id = $_POST['type_id'];
        $get_size_by_type = $this->M_Datamaster->getSizeByType($type_id);
        if (count($get_size_by_type) > 0):
            foreach ($get_size_by_type as $row):
                $val = $row->size_id;
            endforeach;
        else:
            $val = '';
        endif;
        echo $val;
    }

    public function getPrice() {
        $service_id = $_POST['service_id'];
        $size_id = $_POST['size_id'];
        $company_id = $this->session->userdata('company_id');
        if ($company_id == 1) {
            $get_price = $this->M_Datamaster->getPrice($service_id, $size_id);
            if (count($get_price) > 0):
                foreach ($get_price as $row):
                    $prices = str_replace(',', '.', number_format($row->price));
                    $arr = array(
                        'status' => 'success',
                        'price_id' => $row->id_price,
                        'price' => 'Rp. ' . $prices,
                        'message' => ''
                    );
                endforeach;
            else:
                $arr = array(
                    'status' => 'failed',
                    'price_id' => '',
                    'price' => 'Rp. ',
                    'message' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!'
                );
            endif;
        }else {
            $get_price = $this->M_Datamaster->getAllDataPriceMarginByServiceSize($company_id, $service_id, $size_id);
            if (count($get_price) > 0):
                foreach ($get_price as $row):
                    $total_keseluruhan = $row->price + $row->price_total;
                    $hargaAwal = number_format($total_keseluruhan);
//                    $prices = str_replace(',', '.', number_format($row->price));
                    $arr = array(
                        'status' => 'success',
                        'price_id' => $row->id_price,
                        'price' => 'Rp. ' . $hargaAwal,
                        'message' => ''
                    );
                endforeach;
            else:
                $arr = array(
                    'status' => 'failed',
                    'price_id' => '',
                    'price' => 'Rp. ',
                    'message' => 'Harga belum diatur untuk type mobil dan service tersebut, laporkan kepada admin!'
                );
            endif;
        }
        echo json_encode($arr);
    }

    public function bay() {
        $company_id = $this->session->userdata('company_id');
        $this->data['bay'] = $this->M_Datamaster->getAllBayByCompany($company_id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_bay';
        $this->load->view('list_bay', $this->data);
    }

    public function addBay() {
        $this->data['company_id'] = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_bay';
        $this->load->view('add_bay', $this->data);
    }

    public function insertBay() {
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_bay';
        $data = array(
            'name' => $this->input->post('name'),
            'is_active' => 1,
            'company_id' => $this->input->post('company_id')
        );
        $insert = $this->M_Datamaster->insertBay($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Bay berhasil ditambahkan');
            return redirect('datamaster/bay');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/bay');
        }
    }

    public function editBay($id) {
        $this->data['bay'] = $this->M_Datamaster->getBayById($id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_bay';
        $this->load->view('edit_bay', $this->data);
    }

    public function updateBay() {
        $id = $this->input->post('bay_id');
        $data = array(
            'name' => $this->input->post('name'),
        );

        $update = $this->M_Datamaster->updateBay($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Bay berhasil Diubah');
            return redirect('datamaster/bay');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/bay');
        }
    }

    public function deleteBay($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateBay($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Bay berhasil Dihapus');
            return redirect('datamaster/bay');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/bay');
        }
    }

    public function rekening() {
        $company_id = $this->session->userdata('company_id');
        $this->data['rekening'] = $this->M_Datamaster->getAllRekeningByCompany($company_id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_rekening';
        $this->load->view('list_rekening', $this->data);
    }

    public function addRekening() {
        $this->data['company_id'] = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_rekening';
        $this->load->view('add_rekening', $this->data);
    }

    public function insertRekening() {
        $company_id = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_rekening';
        if (!is_dir('./assets/images/rekening/' . $company_id)) {
            mkdir('./assets/images/rekening/' . $company_id, 0777, TRUE);
        }
        if (!empty($_FILES['image_bank']['name'])) {
            $file_ext = pathinfo($_FILES['image_bank']['name'], PATHINFO_EXTENSION);
            $fileName = strtotime(date('Y-m-d H:i:s')) . '.' . $file_ext;
            $config['upload_path'] = './assets/images/rekening/' . $company_id . '/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 10240;
            $config['file_name'] = $fileName;
            $this->load->library('upload');

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_bank')) {
                $this->upload->data();
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Terjadi kesalahan ketika upload gambar, harap periksa file pastikan ekstensionnya : JPG, JPEG, PNG');
                return redirect('datamaster/rekening');
            }
            $image_name = $fileName;
        } else {
            $image_name = '';
        }
        $data = array(
            'no' => $this->input->post('no'),
            'nama_bank' => $this->input->post('nama_bank'),
            'atas_nama' => $this->input->post('atas_nama'),
            'image_bank' => $image_name,
            'company_id' => $company_id
        );
        $insert = $this->M_Datamaster->insertRekening($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Rekening berhasil ditambahkan');
            return redirect('datamaster/rekening');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/rekening');
        }
    }

    public function editRekening($id) {
        $this->data['rekening'] = $this->M_Datamaster->getRekeningById($id);
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_rekening';
        $this->load->view('edit_rekening', $this->data);
    }

    public function updateRekening() {
        $company_id = $this->session->userdata('company_id');
        $id = $this->input->post('id');

        if (!empty($_FILES['image_bank']['name'])) {
            $file_ext = pathinfo($_FILES['image_bank']['name'], PATHINFO_EXTENSION);
            $fileName = strtotime(date('Y-m-d H:i:s')) . '.' . $file_ext;
            $config['upload_path'] = './assets/images/rekening/' . $company_id . '/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 10240;
            $config['file_name'] = $fileName;
            $this->load->library('upload');

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image_bank')) {
                $this->upload->data();
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Terjadi kesalahan ketika upload gambar, harap periksa file pastikan ekstensionnya : JPG, JPEG, PNG');
                return redirect('datamaster/rekening');
            }
            $image_name = $fileName;
        } else {
            $image_name = $this->input->post('image_bank_old');
        }
        $data = array(
            'no' => $this->input->post('no'),
            'nama_bank' => $this->input->post('nama_bank'),
            'atas_nama' => $this->input->post('atas_nama'),
            'image_bank' => $image_name
        );

        $update = $this->M_Datamaster->updateRekening($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Rekening berhasil Diubah');
            return redirect('datamaster/rekening');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/rekening');
        }
    }

    public function deleteRekening($id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateRekening($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Rekening berhasil Dihapus');
            return redirect('datamaster/rekening');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/rekening');
        }
    }

    public function addMargin($company_id) {
        $this->data['company_id'] = $company_id;
        $this->data['data_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_margin';
        $this->load->view('add_margin', $this->data);
    }

    public function insertMargin() {
        $data = array(
            'service_id' => $this->input->post('id_service'),
            'company_id' => $this->input->post('company_id'),
            'margin' => $this->input->post('margin')
        );
        $insert = $this->M_Datamaster->insertMargin($data);
        if ($insert == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Margin berhasil ditambahkan');
            return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
        }
    }

    public function editMargin($margin_id, $company_id) {
        $this->data['company_id'] = $company_id;
        $this->data['margin'] = $this->M_Datamaster->getMarginById($margin_id);
        $this->data['data_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['company_id'] = $company_id;
        $this->data['menuActiveParent'] = 'datamaster';
        $this->data['menuActive'] = 'datamaster_margin';
        $this->load->view('edit_margin', $this->data);
    }

    public function updateMargin() {
        $id = $this->input->post('id');
        $data = array(
            'service_id' => $this->input->post('id_service'),
            'margin' => $this->input->post('margin')
        );

        $update = $this->M_Datamaster->updateMargin($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Margin berhasil Diubah');
            return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/editCompany/' . $this->input->post('company_id'));
        }
    }

    public function deleteMargin($id, $company_id) {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s'),
        );
        $update = $this->M_Datamaster->updateMargin($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Margin berhasil Dihapus');
            return redirect('datamaster/editCompany/' . $company_id);
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('datamaster/editCompany/' . $company_id);
        }
    }

}
