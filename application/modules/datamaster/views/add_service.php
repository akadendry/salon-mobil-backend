<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Service</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_satu').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLDua(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_dua').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLTiga(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_tiga').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLEmpat(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_empat').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <form method="post" action="<?= base_url('datamaster/insertService') ?>" class="form-horizontal" id="sample-form" enctype="multipart/form-data">
                    <input type="hidden" name="type_booking" value="datang"/>
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Home</a>
                            </li>

                            <li>
                                <a href="<?= base_url('precheck') ?>">Data Master</a>
                            </li>

                            <li>
                                <a href="#">Add Service</a>
                            </li>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <div class="page-content">
                        <?php if ($this->session->flashdata('message')) : ?>
                            <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="page-header" style="text-align: center;">
                            <h1 style="color: black;">
                                <b>Add Service</b>
                            </h1>

                        </div>
                        <div class="widget-box">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="fuelux-wizard-container">
                                        <div class="step-content pos-rel">
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Nama</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="name_service" class="width-100" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Deskripsi</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <textarea name="service_desc" class="form-control" required></textarea>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Scope Service</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="scope_service" class="width-100" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Duration Time</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="number" name="duration_time" class="width-100" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Duration Day</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="number" name="duration_day" class="width-100" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">&nbsp;</label>
                                                <div class="col-sm-2" style="text-align: center">
                                                    <button type="submit" class="btn btn-primary btn-block mt-3" style="border-radius: 8px;"><span class="fa fa-save"></span> Simpan</button>
                                                </div>
                                                <div class="col-sm-2" style="text-align: center">
                                                    <a href="<?= base_url('datamaster/type') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;">Kembali</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div>
                    </div>
                </form>
            </div><!-- /.main-content -->


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
                                                        if ('ontouchstart' in document.documentElement)
                                                            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.buttons.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.flash.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.html5.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.print.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.colVis.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.select.min.js') ?>"></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?>"></script>
        <script>
                                                        jQuery(function ($) {
                                                            if (!ace.vars['touch']) {
                                                                $('.chosen-select').chosen({
                                                                    allow_single_deselect: true
                                                                });
                                                                //resize the chosen on window resize

                                                                $(window)
                                                                        .off('resize.chosen')
                                                                        .on('resize.chosen', function () {
                                                                            $('.chosen-select').each(function () {
                                                                                var $this = $(this);
                                                                                $this.next().css({
                                                                                    'width': $this.parent().width()
                                                                                });
                                                                            })
                                                                        }).trigger('resize.chosen');
                                                                //resize chosen on sidebar collapse/expand
                                                                $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
                                                                    if (event_name != 'sidebar_collapsed')
                                                                        return;
                                                                    $('.chosen-select').each(function () {
                                                                        var $this = $(this);
                                                                        $this.next().css({
                                                                            'width': $this.parent().width()
                                                                        });
                                                                    })
                                                                });
                                                            }
                                                        });

                                                        $('a').click(function (e) {
                                                            if ($(this).attr('href') === '#user_yet') {
                                                                e.preventDefault();
                                                                document.getElementById("user_yet").style.display = 'block';
                                                                document.getElementById("user_new").style.display = 'none';
                                                                document.getElementById("id_users").required = true;
                                                                document.getElementById("nama_customer").required = false;
                                                                document.getElementById("email").required = false;
                                                                document.getElementById("nama_customer").value = '';
                                                                document.getElementById("email").value = '';
                                                            } else if ($(this).attr('href') === '#user_new') {
                                                                e.preventDefault();
                                                                document.getElementById("user_yet").style.display = 'none';
                                                                document.getElementById("user_new").style.display = 'block';
                                                                document.getElementById("id_users").required = false;
                                                                document.getElementById("id_users").value = '';
                                                                document.getElementById("nama_customer").required = true;
                                                                document.getElementById("email").required = true;
                                                            }
                                                        });
        </script>
        <script>
            $(document).ready(function () {
                $('#merk').on('change', function() {
                    var merk_id = this.value;
                    $.ajax({
                         url: "<?= base_url('datamaster/getTypeCarByBrand') ?>",
                         type: "POST",
                         data: {
                            merk_id: merk_id
                         },
                         cache: false,
                         success: function(result){
                            $("#type").html(result);
                         }
                    });
                });
                
                $('#type').on('change', function() {
                    var type_id = this.value;
                    $.ajax({
                         url: "<?= base_url('datamaster/getSizeByType') ?>",
                         type: "POST",
                         data: {
                            type_id: type_id
                         },
                         cache: false,
                         success: function(result){
                            $("#size").val(result);
                         }
                    });
                });
                
                $('#jenis_pekerjaan').on('change', function() {
                    var size_id =  $('#size').val();
                    var service_id = this.value;
                    if( $('#type').val() == ''){
                        alert('Silahkan pilih Jenis dan Type Mobil Terlebih Dahulu');
                        document.getElementById("jenis_pekerjaan").value = "";
                    }else{
                        $.ajax({
                             url: "<?= base_url('datamaster/getPrice') ?>",
                             type: "POST",
                             data: {
                                service_id: service_id,
                                size_id: size_id
                             },
                             cache: false,
                             success: function(result){
                                data = JSON.parse(result);
                                console.log(data);
                                var status = data.status;
                                if(status == 'success'){
                                    $("#price_id").val(data.price_id);
                                    $("#price").val(data.price);
                                }else{
                                    $("#price_id").val('');
                                    $("#price").val(data.message);
                                    document.getElementById("jenis_pekerjaan").value = "";
                                }
                             }
                        });
                    }
                });
                
                function myFunction(value, index, array) {
                  txt += value + "<br>";
                }
                
                $('#ban_cadangan_masuk').change(function () {
                    var ban_cadangan_masuk = $('#ban_cadangan_masuk').val();
                    $("div#ban_cadangan_keluar select").val(ban_cadangan_masuk);
                });

                $('#airConditioning_masuk').change(function () {
                    var airConditioning_masuk = $('#airConditioning_masuk').val();
                    $("div#airConditioning_keluar select").val(airConditioning_masuk);
                });

                $('#bautDerek_masuk').change(function () {
                    var bautDerek_masuk = $('#bautDerek_masuk').val();
                    $("div#bautDerek_keluar select").val(bautDerek_masuk);
                });

                $('#antena_masuk').change(function () {
                    var antena_masuk = $('#antena_masuk').val();
                    $("div#antena_keluar select").val(antena_masuk);
                });

                $('#bukuService_masuk').change(function () {
                    var bukuService_masuk = $('#bukuService_masuk').val();
                    $("div#bukuService_keluar select").val(bukuService_masuk);
                });

                $('#floodback_masuk').change(function () {
                    var floodback_masuk = $('#floodback_masuk').val();
                    $("div#floodback_keluar select").val(floodback_masuk);
                });

                $('#pinRadion_masuk').change(function () {
                    var pinRadion_masuk = $('#pinRadion_masuk').val();
                    $("div#pinRadion_keluar select").val(pinRadion_masuk);
                });

                $('#airRadiator_masuk').change(function () {
                    var airRadiator_masuk = $('#airRadiator_masuk').val();
                    $("div#airRadiator_keluar select").val(airRadiator_masuk);
                });

                $('#dongkrak_masuk').change(function () {
                    var dongkrak_masuk = $('#dongkrak_masuk').val();
                    $("div#dongkrak_keluar select").val(dongkrak_masuk);
                });

                $('#ketinggianBrake_masuk').change(function () {
                    var ketinggianBrake_masuk = $('#ketinggianBrake_masuk').val();
                    $("div#ketinggianBrake_keluar select").val(ketinggianBrake_masuk);
                });

                $('#dopTutupRoda_masuk').change(function () {
                    var dopTutupRoda_masuk = $('#dopTutupRoda_masuk').val();
                    $("div#dopTutupRoda_keluar select").val(dopTutupRoda_masuk);
                });

                $('#ketinggianEngine_masuk').change(function () {
                    var ketinggianEngine_masuk = $('#ketinggianEngine_masuk').val();
                    $("div#ketinggianEngine_keluar select").val(ketinggianEngine_masuk);
                });

                $('#tasP3k_masuk').change(function () {
                    var tasP3k_masuk = $('#tasP3k_masuk').val();
                    $("div#tasP3k_keluar select").val(tasP3k_masuk);
                });

                $('#ketinggianPowerSteering_masuk').change(function () {
                    var ketinggianPowerSteering_masuk = $('#ketinggianPowerSteering_masuk').val();
                    $("div#ketinggianPowerSteering_keluar select").val(ketinggianPowerSteering_masuk);
                });

                $('#dudukanPlat_masuk').change(function () {
                    var dudukanPlat_masuk = $('#dudukanPlat_masuk').val();
                    $("div#dudukanPlat_keluar select").val(dudukanPlat_masuk);
                });

                $('#lampuInstrumen_masuk').change(function () {
                    var lampuInstrumen_masuk = $('#lampuInstrumen_masuk').val();
                    $("div#lampuInstrumen_keluar select").val(lampuInstrumen_masuk);
                });

                $('#jokDepanBelakang_masuk').change(function () {
                    var jokDepanBelakang_masuk = $('#jokDepanBelakang_masuk').val();
                    $("div#jokDepanBelakang_keluar select").val(jokDepanBelakang_masuk);
                });

                $('#lampuBelokDarurat_masuk').change(function () {
                    var lampuBelokDarurat_masuk = $('#lampuBelokDarurat_masuk').val();
                    $("div#lampuBelokDarurat_keluar select").val(lampuBelokDarurat_masuk);
                });

                $('#kacaSpionLuar_masuk').change(function () {
                    var kacaSpionLuar_masuk = $('#kacaSpionLuar_masuk').val();
                    $("div#kacaSpionLuar_masuk select").val(kacaSpionLuar_masuk);
                });

                $('#lampuDalam_masuk').change(function () {
                    var lampuDalam_masuk = $('#lampuDalam_masuk').val();
                    $("div#lampuDalam_keluar select").val(lampuDalam_masuk);
                });

                $('#kacaSpionDalam_masuk').change(function () {
                    var kacaSpionDalam_masuk = $('#kacaSpionDalam_masuk').val();
                    $("div#kacaSpionDalam_keluar select").val(kacaSpionDalam_masuk);
                });

                $('#lampuDalamKecil_masuk').change(function () {
                    var lampuDalamKecil_masuk = $('#lampuDalamKecil_masuk').val();
                    $("div#lampuDalamKecil_keluar select").val(lampuDalamKecil_masuk);
                });

                $('#kunciRemotKontak_masuk').change(function () {
                    var kunciRemotKontak_masuk = $('#kunciRemotKontak_masuk').val();
                    $("div#kunciRemotKontak_keluar select").val(kunciRemotKontak_masuk);
                });

                $('#lampuKabut_masuk').change(function () {
                    var lampuKabut_masuk = $('#lampuKabut_masuk').val();
                    $("div#lampuKabut_keluar select").val(lampuKabut_masuk);
                });

                $('#penutupKacaBelakang_masuk').change(function () {
                    var penutupKacaBelakang_masuk = $('#penutupKacaBelakang_masuk').val();
                    $("div#penutupKacaBelakang_keluar select").val(penutupKacaBelakang_masuk);
                });

                $('#lampuRemParkirMundur_masuk').change(function () {
                    var lampuRemParkirMundur_masuk = $('#lampuRemParkirMundur_masuk').val();
                    $("div#lampuRemParkirMundur_keluar select").val(lampuRemParkirMundur_masuk);
                });

                $('#kacaSpionLuar_masuk').change(function () {
                    var kacaSpionLuar_masuk = $('#kacaSpionLuar_masuk').val();
                    $("div#kacaSpionLuar_keluar select").val(kacaSpionLuar_masuk);
                });

                $('#penyulutRoko_masuk').change(function () {
                    var penyulutRoko_masuk = $('#penyulutRoko_masuk').val();
                    $("div#penyulutRoko_keluar select").val(penyulutRoko_masuk);
                });

                $('#powerWindow_masuk').change(function () {
                    var powerWindow_masuk = $('#powerWindow_masuk').val();
                    $("div#powerWindow_keluar select").val(powerWindow_masuk);
                });

                $('#sayapBumperDepan_masuk').change(function () {
                    var sayapBumperDepan_masuk = $('#sayapBumperDepan_masuk').val();
                    $("div#sayapBumperDepan_keluar select").val(sayapBumperDepan_masuk);
                });

                $('#radioTape_masuk').change(function () {
                    var radioTape_masuk = $('#radioTape_masuk').val();
                    $("div#radioTape_keluar select").val(radioTape_masuk);
                });

                $('#segitigaPengaman_masuk').change(function () {
                    var segitigaPengaman_masuk = $('#segitigaPengaman_masuk').val();
                    $("div#segitigaPengaman_keluar select").val(segitigaPengaman_masuk);
                });

                $('#cdPlayerMagazine_masuk').change(function () {
                    var cdPlayerMagazine_masuk = $('#cdPlayerMagazine_masuk').val();
                    $("div#cdPlayerMagazine_keluar select").val(cdPlayerMagazine_masuk);
                });

                $('#sensorParkirMundur_masuk').change(function () {
                    var sensorParkirMundur_masuk = $('#sensorParkirMundur_masuk').val();
                    $("div#sensorParkirMundur_keluar select").val(sensorParkirMundur_masuk);
                });

                $('#centralLock_masuk').change(function () {
                    var centralLock_masuk = $('#centralLock_masuk').val();
                    $("div#centralLock_keluar select").val(centralLock_masuk);
                });

                $('#setKarpet_masuk').change(function () {
                    var setKarpet_masuk = $('#setKarpet_masuk').val();
                    $("div#setKarpet_keluar select").val(setKarpet_masuk);
                });

                $('#accu_masuk').change(function () {
                    var accu_masuk = $('#accu_masuk').val();
                    $("div#accu_keluar select").val(accu_masuk);
                });

                $('#payung_masuk').change(function () {
                    var payung_masuk = $('#payung_masuk').val();
                    $("div#payung_keluar select").val(payung_masuk);
                });

                $('#telephone_masuk').change(function () {
                    var telephone_masuk = $('#telephone_masuk').val();
                    $("div#telephone_keluar select").val(telephone_masuk);
                });

                $('#stnk_masuk').change(function () {
                    var stnk_masuk = $('#stnk_masuk').val();
                    $("div#stnk_keluar select").val(stnk_masuk);
                });

                $('#wiperPenyemprotKaca_masuk').change(function () {
                    var wiperPenyemprotKaca_masuk = $('#wiperPenyemprotKaca_masuk').val();
                    $("div#wiperPenyemprotKaca_keluar select").val(wiperPenyemprotKaca_masuk);
                });


            });
        </script>
    </body>

</html>
