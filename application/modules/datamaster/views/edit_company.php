<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Branch</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css') ?>" />

        <style>
            input[type=checkbox]
                {
                    /* Double-sized Checkboxes */
                    -ms-transform: scale(1); /* IE */
                    -moz-transform: scale(1); /* FF */
                    -webkit-transform: scale(1); /* Safari and Chrome */
                    -o-transform: scale(1); /* Opera */
                    transform: scale(2);
                    padding: 10px;
                }

                
        </style>
        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_satu').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLDua(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_dua').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLTiga(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_tiga').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLEmpat(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_empat').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <form method="post" action="<?= base_url('datamaster/updateCompany') ?>" class="form-horizontal" id="sample-form" enctype="multipart/form-data">
                    <input type="hidden" name="type_booking" value="datang"/>
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Home</a>
                            </li>

                            <li>
                                <a href="<?= base_url('precheck') ?>">Data Master</a>
                            </li>

                            <li>
                                <a href="#">Edit Branch</a>
                            </li>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <div class="page-content">
                        <?php if ($this->session->flashdata('message')) : ?>
                            <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="page-header" style="text-align: center;">
                            <h1 style="color: black;">
                                <b>Edit Branch</b>
                            </h1>

                        </div>
                        <div class="widget-box">
                            <?php foreach($company as $row):?>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="fuelux-wizard-container">
                                        <div class="step-content pos-rel">
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Nama</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="name" value="<?= $row->name ?>" class="width-100 form-control" required />
                                                        <input type="hidden" name="id" value="<?= $row->id ?>" class="width-100 form-control" required />
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Alamat</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <textarea name="address" class="width-100" required><?= $row->address ?></textarea>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Instagram</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="instagram" value="<?= $row->instagram ?>" class="width-100" required placeholder="Example : @dsg.indonesia "/>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Email</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="email" name="email" value="<?= $row->email ?>" class="width-100" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Phone</label>

                                                <div class="col-sm-4">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="number" name="phone" class="width-100" value="<?= $row->phone ?>" required />
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <fieldset class="content-group col-md-12">
                                                <br>
                                                <div class="panel panel-flat">
                                                    <ul class="nav nav-tabs nav-tabs-bottom">
                                                        <?php if($company_id == 1):?>
                                                            <?php $width = 'style="width: 50%;"';?>
                                                        <?php else:?>
                                                            <?php $width = 'style="width: 33%;"';?>
                                                        <?php endif;?>
                                                        <li class="active" <?= $width ?>><a href="#basic-rounded-justified-tab1" data-toggle="tab" aria-expanded="true">Menu</a></li>
                                                        <li class="" <?= $width ?>><a href="#basic-rounded-justified-tab2" data-toggle="tab" aria-expanded="false">User</a></li>
                                                        <?php if($company_id != 1):?>
                                                            <li class="" style="width: 34%;"><a href="#basic-rounded-justified-tab3" data-toggle="tab" aria-expanded="false">Margin</a></li>                                                        
                                                        <?php endif;?>
                                                    </ul>
                                                    <div class="panel-body">
                                                        <div class="tabbable">
                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="basic-rounded-justified-tab1">
                                                                    <table style="border: 0px;">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align: left; width: 30%;">Menu</th>
                                                                                <th style="text-align: center;">Access</th>
                                                                            </tr>
                                                                        </thead>

                                                                        <tbody style="font-size: 14px; font-family: sans-serif;">
                                                                            <?php foreach ($data_menu_access as $dma): ?>
                                                                                <tr>
                                                                                    <td style="width: 87%;">
                                                                                        <ul style="list-style-type: none; margin: 0; padding: 25px;">
                                                                                            <li>
                                                                                                <b><?= $dma['name'] ?></b>
                                                                                                <?php if(count($dma['child'] > 0)):?>
                                                                                                    <?php foreach ($dma['child'] as $rowsss):?>
                                                                                                        <ul style="list-style-type: none; margin: 0; padding: 25px;">
                                                                                                            <li>
                                                                                                                <?= $rowsss->name ?>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    <?php endforeach;?>
                                                                                                <?php endif;?>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </td>
                                                                                    <td style="text-align: center;">
                                                                                        <?php $nama_menu_parent = "menu[".$dma['menu_id']."]"?>
                                                                                        <input name="<?= $nama_menu_parent ?>" style="padding-bottom: 20px;" type="hidden" value="0">
                                                                                        <input name="<?= $nama_menu_parent ?>" style="padding-bottom: 20px;" type="checkbox" <?php if($dma['access'] == 1){ echo "checked";}?>>
                                                                                        <br>
                                                                                        <?php if(count($dma['child'] > 0)):?>
                                                                                            <?php foreach ($dma['child'] as $rowsss):?>
                                                                                            <?php $nama_menu_child = "menu[".$rowsss->menu_id."]"?>
                                                                                                <ul style="list-style-type: none; margin: 0; padding: 25px;">
                                                                                                    <li>
                                                                                                        <input name="<?= $nama_menu_child ?>" style="padding-bottom: 20px;" type="hidden" value="0">
                                                                                                        <input name="<?= $nama_menu_child ?>" type="checkbox" <?php if($rowsss->access == 1){ echo "checked";}?>>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            <?php endforeach;?>
                                                                                        <?php endif;?>
                                                                                    </td>                                                                                    
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                
                                                                <div class="tab-pane" id="basic-rounded-justified-tab2">
                                                                    <div class="row">
                                                                        <?php if ($this->session->userdata("role") == 1): ?>
                                                                            <div class="col-xs-12">
                                                                                <div style="float: right;">
                                                                                    <a href="<?= base_url('datamaster/addUser/'.$company_id) ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <br>
                                                                    <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Name</th>
                                                                                <th>Username/Email</th>
                                                                                <!--<th>Phone</th>-->
                                                                                <th>Role</th>
                                                                                <th>Status</th>
                                                                                <th style="text-align: center;">Action</th>
                                                                                <th hidden>&nbsp;</th>
                                                                            </tr>
                                                                        </thead>

                                                                        <tbody>
                                                                            <?php $no = 1; ?>
                                                                            <?php foreach ($user as $rows): ?>
                                                                                <tr>
                                                                                    <td><?= $no++ ?></td>
                                                                                    <td><?= $rows->name ?></td>
                                                                                    <td><?= $rows->email ?></td>
                                                                                    <!--<td></?= $row->phone ?></td>-->
                                                                                    <td><?= $rows->role_name ?></td>
                                                                                    <?php if ($rows->deleted_at == null || $rows->deleted_at == ''): ?>
                                                                                        <td>Aktif</td>
                                                                                    <?php else: ?>
                                                                                        <td>Tidak Aktif</td>
                                                                                    <?php endif; ?>
                                                                                    <td style="text-align: center">
                                                                                        <?php if ($rows->role != 1): ?>
                                                                                            <a href="<?= base_url('datamaster/editUser/'.$rows->id_user.'/'.$company_id)?>" style="cursor:pointer"><i class="fa fa-pencil"></i></a> ||
                                                                                            <a href="<?= base_url('datamaster/deleteUser/'.$rows->id_user.'/'.$company_id)?>" onclick="return confirm('Are you sure delete this data?')" style="cursor:pointer"><i class="fa fa-trash"></i></a>
                                                                                        <?php endif; ?>

                                                                                    </td>
                                                                                    <td hidden>&nbsp;</td>
                                                                                </tr>
                                                                            <?php endforeach; ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                
                                                                <?php if($company_id != 1):?>
                                                                    <div class="tab-pane" id="basic-rounded-justified-tab3">
                                                                        <div class="row">
                                                                            <?php if ($this->session->userdata("role") == 1): ?>
                                                                                <div class="col-xs-12">
                                                                                    <div style="float: right;">
                                                                                        <a href="<?= base_url('datamaster/addMargin/'.$company_id) ?>" class="btn btn-success"><span class="fa fa-plus"></span> Tambah</a>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <br>
                                                                        <table id="dynamic-table2" class="table table-striped table-bordered table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>No</th>
                                                                                    <th>Name Service</th>
                                                                                    <th>Margin</th>
                                                                                    <th style="text-align: center;">Action</th>
                                                                                    <th hidden>&nbsp;</th>
                                                                                    <th hidden>&nbsp;</th>
                                                                                    <th hidden>&nbsp;</th>
                                                                                </tr>
                                                                            </thead>

                                                                            <tbody>
                                                                                <?php $no = 1; ?>
                                                                                <?php foreach ($margin as $mr): ?>
                                                                                    <tr>
                                                                                        <td><?= $no++ ?></td>
                                                                                        <td><?= $mr->name_service ?></td>
                                                                                        <td><?= $mr->margin ?></td>
                                                                                        <td style="text-align: center">
                                                                                            <a href="<?= base_url('datamaster/editMargin/'.$mr->id.'/'.$company_id)?>" style="cursor:pointer"><i class="fa fa-pencil"></i></a> ||
                                                                                            <a href="<?= base_url('datamaster/deleteMargin/'.$mr->id.'/'.$company_id)?>" onclick="return confirm('Are you sure delete this data?')" style="cursor:pointer"><i class="fa fa-trash"></i></a>                                                                                       
                                                                                        </td>
                                                                                        <td hidden>&nbsp;</td>
                                                                                        <td hidden>&nbsp;</td>
                                                                                        <td hidden>&nbsp;</td>
                                                                                    </tr>
                                                                                <?php endforeach; ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                <?php endif;?>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            
                                            <div class="form-group">
                                                <label for="input" class="col-sm-12 no-padding-right">&nbsp;</label>
                                                
                                                <div class="col-sm-6" style="text-align: center">
                                                    <a href="<?= base_url('datamaster/company') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;">Kembali</a>
                                                </div>
                                                <div class="col-sm-6" style="text-align: center">
                                                    <button type="submit" class="btn btn-primary btn-block mt-3" style="border-radius: 8px;"><span class="fa fa-save"></span> Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                            <?php endforeach;?>
                        </div>
                    </div>
                </form>
            </div><!-- /.main-content -->
            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
                                                        if ('ontouchstart' in document.documentElement)
                                                            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.buttons.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.flash.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.html5.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.print.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.colVis.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.select.min.js') ?>"></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?>"></script>
        
        <script type="text/javascript">
            jQuery(function ($) {
                //initiate dataTables plugin
                var myTable =
                        $('#dynamic-table')
                        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                        .DataTable({
                            bAutoWidth: false,
                            "aoColumns": [
                                {"bSortable": false},
                                null, null, null, null, null,
                                {"bSortable": false}
                            ],
                            "aaSorting": [],
                            
                            select: {
                                style: 'multi'
                            }
                        });



                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                new $.fn.dataTable.Buttons(myTable, {
                    buttons: [
                        {
                            "extend": "colvis",
                            "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            columns: ':not(:first):not(:last)'
                        },
                        {
                            "extend": "copy",
                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "csv",
                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "excel",
                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "pdf",
                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "print",
                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            autoPrint: false,
                            message: 'This print was produced using the Print button for DataTables'
                        }
                    ]
                });
                myTable.buttons().container().appendTo($('.tableTools-container'));

                //style the message box
                var defaultCopyAction = myTable.button(1).action();
                myTable.button(1).action(function (e, dt, button, config) {
                    defaultCopyAction(e, dt, button, config);
                    $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                });


                var defaultColvisAction = myTable.button(0).action();
                myTable.button(0).action(function (e, dt, button, config) {

                    defaultColvisAction(e, dt, button, config);


                    if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                        $('.dt-button-collection')
                                .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                .find('a').attr('href', '#').wrap("<li />")
                    }
                    $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                });

                ////

                setTimeout(function () {
                    $($('.tableTools-container')).find('a.dt-button').each(function () {
                        var div = $(this).find(' > div').first();
                        if (div.length == 1)
                            div.tooltip({container: 'body', title: div.parent().text()});
                        else
                            $(this).tooltip({container: 'body', title: $(this).text()});
                    });
                }, 500);




//
//                myTable.on('select', function (e, dt, type, index) {
//                    if (type === 'row') {
//                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
//                    }
//                });
//                myTable.on('deselect', function (e, dt, type, index) {
//                    if (type === 'row') {
//                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
//                    }
//                });




                /////////////////////////////////
                //table checkboxes
//                $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

                //select/deselect all rows according to table header checkbox
//                $('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function () {
//                    var th_checked = this.checked;//checkbox inside "TH" table header
//
//                    $('#dynamic-table').find('tbody > tr').each(function () {
//                        var row = this;
//                        if (th_checked)
//                            myTable.row(row).select();
//                        else
//                            myTable.row(row).deselect();
//                    });
//                });
//
//                //select/deselect a row when the checkbox is checked/unchecked
//                $('#dynamic-table').on('click', 'td input[type=checkbox]', function () {
//                    var row = $(this).closest('tr').get(0);
//                    if (this.checked)
//                        myTable.row(row).deselect();
//                    else
//                        myTable.row(row).select();
//                });



                $(document).on('click', '#dynamic-table .dropdown-toggle', function (e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                });



                //And for the first simple table, which doesn't have TableTools or dataTables
                //select/deselect all rows according to table header checkbox
//                var active_class = 'active';
//                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
//                    var th_checked = this.checked;//checkbox inside "TH" table header
//
//                    $(this).closest('table').find('tbody > tr').each(function () {
//                        var row = this;
//                        if (th_checked)
//                            $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
//                        else
//                            $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
//                    });
//                });

                //select/deselect a row when the checkbox is checked/unchecked
//                $('#simple-table').on('click', 'td input[type=checkbox]', function () {
//                    var $row = $(this).closest('tr');
//                    if ($row.is('.detail-row '))
//                        return;
//                    if (this.checked)
//                        $row.addClass(active_class);
//                    else
//                        $row.removeClass(active_class);
//                });



                /********************************/
                //add tooltip for small view action buttons in dropdown menu
                $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

                //tooltip placement on right or left
                function tooltip_placement(context, source) {
                    var $source = $(source);
                    var $parent = $source.closest('table')
                    var off1 = $parent.offset();
                    var w1 = $parent.width();

                    var off2 = $source.offset();
                    //var w2 = $source.width();

                    if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                        return 'right';
                    return 'left';
                }
                $('.show-details-btn').on('click', function (e) {
                    e.preventDefault();
                    $(this).closest('tr').next().toggleClass('open');
                    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                });

            })
        </script>
        
        <script type="text/javascript">
            jQuery(function ($) {
                //initiate dataTables plugin
                var myTable =
                        $('#dynamic-table2')
                        //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                        .DataTable({
                            bAutoWidth: false,
                            "aoColumns": [
                                {"bSortable": false},
                                null, null, null, null, null,
                                {"bSortable": false}
                            ],
                            "aaSorting": [],
                            
                            select: {
                                style: 'multi'
                            }
                        });



                $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

                new $.fn.dataTable.Buttons(myTable, {
                    buttons: [
                        {
                            "extend": "colvis",
                            "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            columns: ':not(:first):not(:last)'
                        },
                        {
                            "extend": "copy",
                            "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "csv",
                            "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "excel",
                            "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "pdf",
                            "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                            "className": "btn btn-white btn-primary btn-bold"
                        },
                        {
                            "extend": "print",
                            "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                            "className": "btn btn-white btn-primary btn-bold",
                            autoPrint: false,
                            message: 'This print was produced using the Print button for DataTables'
                        }
                    ]
                });
                myTable.buttons().container().appendTo($('.tableTools-container'));

                //style the message box
                var defaultCopyAction = myTable.button(1).action();
                myTable.button(1).action(function (e, dt, button, config) {
                    defaultCopyAction(e, dt, button, config);
                    $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
                });


                var defaultColvisAction = myTable.button(0).action();
                myTable.button(0).action(function (e, dt, button, config) {

                    defaultColvisAction(e, dt, button, config);


                    if ($('.dt-button-collection > .dropdown-menu').length == 0) {
                        $('.dt-button-collection')
                                .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                                .find('a').attr('href', '#').wrap("<li />")
                    }
                    $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
                });

                ////

                setTimeout(function () {
                    $($('.tableTools-container')).find('a.dt-button').each(function () {
                        var div = $(this).find(' > div').first();
                        if (div.length == 1)
                            div.tooltip({container: 'body', title: div.parent().text()});
                        else
                            $(this).tooltip({container: 'body', title: $(this).text()});
                    });
                }, 500);





//                myTable.on('select', function (e, dt, type, index) {
//                    if (type === 'row') {
//                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
//                    }
//                });
//                myTable.on('deselect', function (e, dt, type, index) {
//                    if (type === 'row') {
//                        $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
//                    }
//                });
//
//
//
//
//                /////////////////////////////////
//                //table checkboxes
//                $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
//
//                //select/deselect all rows according to table header checkbox
//                $('#dynamic-table2 > thead > tr > th input[type=checkbox], #dynamic-table2_wrapper input[type=checkbox]').eq(0).on('click', function () {
//                    var th_checked = this.checked;//checkbox inside "TH" table header
//
//                    $('#dynamic-table2').find('tbody > tr').each(function () {
//                        var row = this;
//                        if (th_checked)
//                            myTable.row(row).select();
//                        else
//                            myTable.row(row).deselect();
//                    });
//                });
//
//                //select/deselect a row when the checkbox is checked/unchecked
//                $('#dynamic-table2').on('click', 'td input[type=checkbox]', function () {
//                    var row = $(this).closest('tr').get(0);
//                    if (this.checked)
//                        myTable.row(row).deselect();
//                    else
//                        myTable.row(row).select();
//                });



                $(document).on('click', '#dynamic-table2 .dropdown-toggle', function (e) {
                    e.stopImmediatePropagation();
                    e.stopPropagation();
                    e.preventDefault();
                });



                //And for the first simple table, which doesn't have TableTools or dataTables
                //select/deselect all rows according to table header checkbox
//                var active_class = 'active';
//                $('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
//                    var th_checked = this.checked;//checkbox inside "TH" table header
//
//                    $(this).closest('table').find('tbody > tr').each(function () {
//                        var row = this;
//                        if (th_checked)
//                            $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
//                        else
//                            $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
//                    });
//                });
//
//                //select/deselect a row when the checkbox is checked/unchecked
//                $('#simple-table').on('click', 'td input[type=checkbox]', function () {
//                    var $row = $(this).closest('tr');
//                    if ($row.is('.detail-row '))
//                        return;
//                    if (this.checked)
//                        $row.addClass(active_class);
//                    else
//                        $row.removeClass(active_class);
//                });



                /********************************/
                //add tooltip for small view action buttons in dropdown menu
                $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

                //tooltip placement on right or left
                function tooltip_placement(context, source) {
                    var $source = $(source);
                    var $parent = $source.closest('table')
                    var off1 = $parent.offset();
                    var w1 = $parent.width();

                    var off2 = $source.offset();
                    //var w2 = $source.width();

                    if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2))
                        return 'right';
                    return 'left';
                }
                $('.show-details-btn').on('click', function (e) {
                    e.preventDefault();
                    $(this).closest('tr').next().toggleClass('open');
                    $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
                });

            })
        </script>
    </body>

</html>
