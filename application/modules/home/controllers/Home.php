<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->database();
        if($this->session->userdata('email') == NULL){
            return redirect('login');
		}
		date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $this->load->view('V_home');
    }

}
