<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_login');
    }

    public function index() {
        if(!$this->session->userdata('email')){
            $this->data['title_page'] = 'Login Page';
            $this->load->view('V_login', $this->data);
        }else{
            return header("location:javascript://history.go(-1)");
        }
            
    }

    public function process_login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if ($username == '') {
            $this->session->set_flashdata('message', 'Username Tidak Boleh Kosong');
            return redirect('login');
        } elseif ($password == '') {
            $this->session->set_flashdata('message', 'Password Tidak Boleh Kosong');
            return redirect('login');
        }
        $array_data = array(
            'email' => $username,
            'password' => sha1(md5($password))
        );
        $check = $this->M_login->check_login($array_data)->result();
        if (count($check) > 0) {
            foreach ($check as $row):
                $company_id = $row->company_id;
                $session_data = array(
                    'id' => $row->id_user,
                    'name' => $row->name,
                    'email' => $row->email,
                    'role' => $row->role,
                    'company_id' => $row->company_id
                );
            endforeach;

            $data_access = array();
            $get_all_menu = $this->M_login->getParrentMenuAccessByCompany($company_id, $session_data['role']);
            if (count($get_all_menu) > 0) {
                foreach ($get_all_menu as $row):
                    $data_menu = array(
                        'id' => $row->id,
                        'company_id' => $row->company_id,
                        'menu_id' => $row->menu_id,
                        'access' => $row->access,
                        'name' => $row->name,
                        'icon' => $row->icon,
                        'url' => $row->url,
                        'slug' => $row->slug,
                        'order' => $row->order
                    );
                    $menu_id = $row->menu_id;
                    $childMenuAccess = $this->M_login->getChildMenuAccessByCompany($company_id, $menu_id);
                    if (count($childMenuAccess) > 0):
                        $data_menu['child'] = $childMenuAccess;
                    endif;
                    array_push($data_access, $data_menu);
                endforeach;
                $session_data['menu'] = $data_access;

                //sampe sini tinggal validasi menu awal aaja
            //    print_r(json_encode($session_data['menu']));
            //    exit();
                $this->session->set_userdata($session_data);
                for($i = 0; $i <= count($session_data['menu']); $i++){
                    if($session_data['menu'][$i]['url'] != '#'){
                        //parent
                        return redirect($session_data['menu'][$i]['url']);
                    }else{
                        //child
                        return redirect($session_data['menu'][$i]['child'][0]->url);
                    }
                }
                //kodingan lama
                // if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) {
                //     return redirect('booking');
                // }
                // return redirect('precheck');
                //selesai kodingan lama
            } else {
                $this->session->set_flashdata('message', 'Perusahaan anda belum didaftarkan hak akses menunya, harap hubungi admin pusat!');
                return redirect('login');
            }
        } else {
            $this->session->set_flashdata('message', 'Username dan Password Salah');
            return redirect('login');
        }
    }

    public function process_logout() {
        session_destroy();
        unset($_SESSION);
        return redirect('login');
    }

    public function changePassword() {
        $this->data['menuActive'] = 'password';
        $this->data['old_password'] = '';
        $this->data['new_password'] = '';
        $this->data['re_password'] = '';
        $this->load->view('change_password', $this->data);
    }

    public function prosessChange() {
        $user_id = $_SESSION['id'];
        $username = $_SESSION['email'];
        $old_password = $this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        $re_password = $this->input->post('re_password');

        if ($new_password !== $re_password) {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Password baru dan ulangi password tidak sama, harap periksa kembali!');
            $this->data['menuActive'] = 'password';
            $this->data['old_password'] = $old_password;
            $this->data['new_password'] = $new_password;
            $this->data['re_password'] = $re_password;
            $this->load->view('change_password', $this->data);
        } else {
            //cek password lama
            $array_data = array(
                'email' => $username,
                'password' => sha1(md5($old_password))
            );
            $check_old_password = $this->M_login->check_login($array_data)->result();
            if (count($check_old_password) > 0) {
                $data = array(
                    'password' => sha1(md5($new_password))
                );
                $update = $this->M_login->updateUser($data, $user_id);
                if ($update == 1) {
                    $this->session->set_flashdata('status', 'success');
                    $this->session->set_flashdata('message', 'Password berhasil Diubah');
                    return redirect('login/changePassword');
                } else {
                    $this->session->set_flashdata('status', 'danger');
                    $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
                    return redirect('login/changePassword');
                }
            } else {
                $this->session->set_flashdata('status', 'danger');
                $this->session->set_flashdata('message', 'Password lama anda salah, harap periksa kembali!');
                $this->data['menuActive'] = 'password';
                $this->data['old_password'] = $old_password;
                $this->data['new_password'] = $new_password;
                $this->data['re_password'] = $re_password;
                $this->load->view('change_password', $this->data);
            }
        }
    }

}
