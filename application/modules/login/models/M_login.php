<?php

class M_login extends CI_Model {

    function check_login($where) {
        return $this->db->get_where('mst_users_cms',$where);
    }
    
    public function updateUser($data, $user_id){
        $this->db->where('id_user', $user_id);
        $query = $this->db->update('mst_users_cms', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function getParrentMenuAccessByCompany($id, $role) {
        if($role == 2):
            $condition = "AND slug NOT IN ('booking','datamaster','branch', 'report')";
        else:
            $condition = "";
        endif;
        $data = $this->db->query("SELECT a.*, b.id as company_id, b.name as nama_company, c.parentMenuId, c.subParentMenuId, c.name, c.icon, c.url, c.order, c.slug
                                  FROM menu_access a
                                  JOIN mst_company b ON a.company_id = b.id
                                  JOIN menu c ON a.menu_id = c.id
                                  WHERE a.company_id = '$id' AND c.parentMenuId is NULL AND a.access = 1 $condition");
        return $data->result();
    }
    
    public function getChildMenuAccessByCompany($id, $menu_id) {
        $data = $this->db->query("SELECT a.*, b.id as company_id, b.name as nama_company, c.parentMenuId, c.subParentMenuId, c.name, c.icon, c.url, c.order, c.slug
                                  FROM menu_access a
                                  JOIN mst_company b ON a.company_id = b.id
                                  JOIN menu c ON a.menu_id = c.id
                                  WHERE a.company_id = '$id' AND c.parentMenuId is NOT NULL AND c.parentMenuId = '$menu_id' AND a.access = 1");
        return $data->result();
    }

}
