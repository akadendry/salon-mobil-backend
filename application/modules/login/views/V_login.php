<?php $this->load->view('v_header'); ?>
<body class="login-layout" style="background-image:url(url(../images/bg2.png))">
    <div class="main-container">
        <div class="main-content">
            <div class="row" style="padding-top: 130px;">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="center">
                            <h1>
                                <a href="#"><img class="logo" style="width: 25%;" src="<?php echo base_url();?>assets/images/logo/logo_abu.png"></a>
                                <span class="blue">Dashboard Login</span>
                                <!--<span class="blue" id="id-text2">Salon Mobil</span>-->
                            </h1>
                            <!--<h4 class="blue" id="id-company-text">&copy; Company Name</h4>-->
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="ace-icon fa fa-lock blue"></i>
                                            Login untuk masuk ke system
                                        </h4>
                                        <div class="space-6"></div>
                                        <?php if ($this->session->flashdata('message')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo $this->session->flashdata('message'); ?>
                                        </div>
                                        <?php endif;?>
                                        <form action="<?php echo base_url(). 'login/process_login'; ?>" method="post">
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" name="username" class="form-control" placeholder="Username" />
                                                        <i class="ace-icon fa fa-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" name="password" class="form-control" placeholder="Password" />
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>

                                                <div class="space"></div>

                                                <div class="clearfix">
<!--                                                    <label class="inline">
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl"> Remember Me</span>
                                                    </label>-->

                                                    <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                        <i class="ace-icon fa fa-key"></i>
                                                        <span class="bigger-110">Login</span>
                                                    </button>
                                                </div>

                                                <div class="space-4"></div>
                                            </fieldset>
                                        </form>
                                    </div><!-- /.widget-main -->

                                    
                            </div><!-- /.login-box -->
                        </div><!-- /.position-relative -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.main-content -->
    </div><!-- /.main-container -->

    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="assets/js/jquery-2.1.4.min.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement)
            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
</body>
</html>
