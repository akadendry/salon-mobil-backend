<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Precheck extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Precheck');
        $this->load->model('M_Datamaster');
        if ($this->session->userdata('email') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {
        $company_id = $this->session->userdata('company_id');
        $this->data['list_precheck'] = $this->M_Precheck->get_list_precheck($company_id);
        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';
        $this->load->view('list_precheck', $this->data);
    }

    public function add() {
        $company_id = $this->session->userdata('company_id');
        $this->data['listCompany'] = $this->M_Datamaster->getAllDataCompany();
        $this->data['list_users'] = $this->M_Precheck->get_list_users();
        if ($company_id == 1) {
            $this->data['list_service'] = $this->M_Datamaster->getAllDataService();
        } else {
            $this->data['list_service'] = $this->M_Datamaster->getAllDataServiceMerge($company_id);
        }
        $this->data['list_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['list_type'] = $this->M_Datamaster->getAllDataType();
        $this->data['company_id'] = $company_id;
        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';
        $this->load->view('precheck', $this->data);
    }

    public function insert() {
        $no_polisi = str_replace(' ', '-', $this->input->post('no_polisi'));
        if (!is_dir('./assets/images/precheck/' . $no_polisi)) {
            mkdir('./assets/images/precheck/' . $no_polisi, 0777, TRUE);
        }
        $array_image = array();

        if ($this->input->post('type_booking') == 'datang') {
            //validasi gambar plus insert gambar
            if (!empty($_FILES['tampak_satu']['name'])) {
                $file_ext = pathinfo($_FILES['tampak_satu']['name'], PATHINFO_EXTENSION);
                $fileName = strtotime(date('Y-m-d H:i:s')) . '_tampakSatu.' . $file_ext;
                $this->uploadImagePrecheck($fileName, $no_polisi, null, $this->input->post('type_booking'));
                $image['tampak_satu'] = $fileName;
            } else {
                $image['tampak_satu'] = '';
            }

            if (!empty($_FILES['tampak_dua']['name'])) {
                $file_extTampakDua = pathinfo($_FILES['tampak_dua']['name'], PATHINFO_EXTENSION);
                $fileNameTampakDua = strtotime(date('Y-m-d H:i:s')) . '_tampakDua.' . $file_extTampakDua;
                $this->uploadImagePrecheckDua($fileNameTampakDua, $no_polisi, null, $this->input->post('type_booking'));
                $image['tampak_dua'] = $fileNameTampakDua;
            } else {
                $image['tampak_dua'] = '';
            }

            if (!empty($_FILES['tampak_tiga']['name'])) {
                $file_extTampakTiga = pathinfo($_FILES['tampak_tiga']['name'], PATHINFO_EXTENSION);
                $fileNameTampakTiga = strtotime(date('Y-m-d H:i:s')) . '_tampakTiga.' . $file_extTampakTiga;
                $this->uploadImagePrecheckTiga($fileNameTampakTiga, $no_polisi, null, $this->input->post('type_booking'));
                $image['tampak_tiga'] = $fileNameTampakTiga;
            } else {
                $image['tampak_tiga'] = '';
            }

            if (!empty($_FILES['tampak_empat']['name'])) {
                $file_extTampakEmpat = pathinfo($_FILES['tampak_empat']['name'], PATHINFO_EXTENSION);
                $fileNameTampakEmpat = strtotime(date('Y-m-d H:i:s')) . '_tampakEmpat.' . $file_extTampakEmpat;
                $this->uploadImagePrecheckEmpat($fileNameTampakEmpat, $no_polisi, null, $this->input->post('type_booking'));
                $image['tampak_empat'] = $fileNameTampakEmpat;
            } else {
                $image['tampak_empat'] = '';
            }

            $price = str_replace("Rp. ", "", $this->input->post('price'));
            $price = str_replace(".", "", $price);
            $price = str_replace(",", "", $price);
            $dp = str_replace('Rp.', '', $this->input->post('dp'));
            $dp = str_replace('.', '', $dp);
            $dp = str_replace(',', '.', $dp);
            $dataBooking = array(
                'id_user' => $this->input->post('id_users'),
                'no_polisi' => $this->input->post('no_polisi'),
                'car_brand' => $this->input->post('merk'),
                'car_type' => $this->input->post('type'),
                'name_service' => $this->input->post('jenis_pekerjaan'),
                'car_color' => $this->input->post('car_color'),
                'book_date' => date('Y-m-d H:i:s'),
                'status_bayar' => 1,
                'id_size' => $this->input->post('size'),
                'id_price' => $this->input->post('price_id'),
                'price' => $price,
                'type_booking' => 'datang',
                'status_inspect' => 'sudah',
                'statusDp' => 1,
                'dp' => $dp,
                'created_by' => $this->session->userdata('id'),
                'created_at' => date('Y-m-d H:i:s'),
                'company_id' => $this->input->post('company_id')
            );
            $idBooking = $this->M_Precheck->insertBooking($dataBooking);
        } else {
            $idBooking = $this->input->post('id_booking');
            if (!empty($_FILES['tampak_satu']['name'])) {
                $file_ext = pathinfo($_FILES['tampak_satu']['name'], PATHINFO_EXTENSION);
                $fileName = strtotime(date('Y-m-d H:i:s')) . '_tampakSatu.' . $file_ext;
                $this->uploadImagePrecheck($fileName, $no_polisi, $idBooking, $this->input->post('type_booking'));
                $image['tampak_satu'] = $fileName;
            } else {
                $image['tampak_satu'] = '';
            }

            if (!empty($_FILES['tampak_dua']['name'])) {
                $file_extTampakDua = pathinfo($_FILES['tampak_dua']['name'], PATHINFO_EXTENSION);
                $fileNameTampakDua = strtotime(date('Y-m-d H:i:s')) . '_tampakDua.' . $file_extTampakDua;
                $this->uploadImagePrecheckDua($fileNameTampakDua, $no_polisi, $idBooking, $this->input->post('type_booking'));
                $image['tampak_dua'] = $fileNameTampakDua;
            } else {
                $image['tampak_dua'] = '';
            }

            if (!empty($_FILES['tampak_tiga']['name'])) {
                $file_extTampakTiga = pathinfo($_FILES['tampak_tiga']['name'], PATHINFO_EXTENSION);
                $fileNameTampakTiga = strtotime(date('Y-m-d H:i:s')) . '_tampakTiga.' . $file_extTampakTiga;
                $this->uploadImagePrecheckTiga($fileNameTampakTiga, $no_polisi, $idBooking, $this->input->post('type_booking'));
                $image['tampak_tiga'] = $fileNameTampakTiga;
            } else {
                $image['tampak_tiga'] = '';
            }

            if (!empty($_FILES['tampak_empat']['name'])) {
                $file_extTampakEmpat = pathinfo($_FILES['tampak_empat']['name'], PATHINFO_EXTENSION);
                $fileNameTampakEmpat = strtotime(date('Y-m-d H:i:s')) . '_tampakEmpat.' . $file_extTampakEmpat;
                $this->uploadImagePrecheckEmpat($fileNameTampakEmpat, $no_polisi, $idBooking, $this->input->post('type_booking'));
                $image['tampak_empat'] = $fileNameTampakEmpat;
            } else {
                $image['tampak_empat'] = '';
            }
            $dataBooking = array(
                'status_inspect' => 'sudah',
            );
            
            $this->M_Precheck->updatedBooking($dataBooking, $idBooking);
        }

        $image['created_at'] = date('Y-m-d H:i:s');
        $image['id_booking'] = $idBooking;

        if ($this->input->post('type_booking') == 'datang') {
            $image['type_booking'] = 'datang';
        }

        $image['created_by'] = $this->session->userdata('id');
        $data = array_merge($_POST, $image);

        //insert data
//        if ($_POST['id_users'] != '') {

        unset($data['company_id'], $data['dp'], $data['status'], $data['id_precheck'], $data['nama_customer'], $data['email'], $data['id_users'], $data['no_polisi'], $data['size'], $data['price'], $data['price_id'], $data['merk'], $data['type'], $data['jenis_pekerjaan'], $data['car_color']);

        if ($this->input->post('type_booking') == 'datang') {
            $sql = $this->M_Precheck->insert($data);

            $customer_id = $this->input->post('id_users');
            $data_customer = $this->M_Datamaster->getDataCustomerById($customer_id);
            foreach ($data_customer as $dc):
                $email_customer = $dc->email;
            endforeach;

            //cetak dan save pdfnya
            $filenameinvoice = strtotime(date('Y-m-d H:i:s')) . ".pdf";
            $this->cetakInoviceDpPdf($idBooking, $filenameinvoice);
            //akhir cetak dan save pdfnya
            //kirim email
            $config = Array(
                'smtp_host' => 'mail.detailingshopgarage.com',
                'smtp_port' => 587,
                'smtp_user' => 'no-reply@detailingshopgarage.com', // change it to yours
                'smtp_auth' => TRUE,
                'smtp_pass' => 'Password1', // change it to yours
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'newline' => "\r\n",
                'wordwrap' => TRUE
            );
            $pdfFilePath = FCPATH . "assets/file/invoice/" . $filenameinvoice;
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('admin@dsgindonesia.com', 'DSG Indonesia');
            $this->email->to($email_customer); // change it to yours
            $this->email->subject('Invoice Shop Garage');
            $this->data['recipient_email'] = $email_customer;
            $this->data['subjek_email'] = "Invoice";
            $email = $this->load->view('v_email_template_invoice', $this->data, TRUE);
            $this->email->message($email);
            $this->email->attach($pdfFilePath);
            $this->email->send();
            //akhir kirim email
        } else {
            $sql = $this->M_Precheck->updatePrecheckByIdbooking($data, $idBooking);
        }

        if ($sql == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Pre Check berhasil ditambahkan');
            return redirect('precheck');
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('precheck');
        }

//        } else {
//            echo 'ini belum jadi';
//            exit();
//        }
    }

    public function insertOnline() {

        if ($this->input->post('type_booking') == 'datang') {

            $price = str_replace("Rp. ", "", $this->input->post('price'));

            $price = str_replace(",", "", $price);

            $dataBooking = array(
                'id_user' => $this->input->post('id_users'),
                'no_polisi' => $this->input->post('no_polisi'),
                'car_brand' => $this->input->post('merk'),
                'car_type' => $this->input->post('type'),
                'name_service' => $this->input->post('jenis_pekerjaan'),
                'car_color' => $this->input->post('car_color'),
                'book_date' => date('Y-m-d H:i:s'),
                'status_bayar' => 1,
                'id_size' => $this->input->post('size'),
                'id_price' => $this->input->post('price_id'),
                'price' => $price,
                'created_by' => $this->session->userdata('id'),
                'created_at' => date('Y-m-d H:i:s')
            );



            $idBooking = $this->M_Precheck->insertBooking($dataBooking);
        } else {

            $idBooking = $this->input->post('id_booking');
        }





        $no_polisi = str_replace(' ', '-', $this->input->post('no_polisi'));

        if (!is_dir('./assets/images/precheck/' . $no_polisi)) {

            mkdir('./assets/images/precheck/' . $no_polisi, 0777, TRUE);
        }



        $array_image = array();

        if (!empty($_FILES['tampak_satu']['name'])) {

            $file_ext = pathinfo($_FILES['tampak_satu']['name'], PATHINFO_EXTENSION);

            $fileName = strtotime(date('Y-m-d H:i:s')) . '_tampakSatu.' . $file_ext;

            $this->uploadImagePrecheck($fileName, $no_polisi, null);

            $image['tampak_satu'] = $fileName;
        } else {

            $image['tampak_satu'] = '';
        }



        if (!empty($_FILES['tampak_dua']['name'])) {

            $file_extTampakDua = pathinfo($_FILES['tampak_dua']['name'], PATHINFO_EXTENSION);

            $fileNameTampakDua = strtotime(date('Y-m-d H:i:s')) . '_tampakDua.' . $file_extTampakDua;

            $this->uploadImagePrecheckDua($fileNameTampakDua, $no_polisi);

            $image['tampak_dua'] = $fileNameTampakDua;
        } else {

            $image['tampak_dua'] = '';
        }

        if (!empty($_FILES['tampak_tiga']['name'])) {

            $file_extTampakTiga = pathinfo($_FILES['tampak_tiga']['name'], PATHINFO_EXTENSION);

            $fileNameTampakTiga = strtotime(date('Y-m-d H:i:s')) . '_tampakTiga.' . $file_extTampakTiga;

            $this->uploadImagePrecheckTiga($fileNameTampakTiga, $no_polisi);

            $image['tampak_tiga'] = $fileNameTampakTiga;
        } else {

            $image['tampak_tiga'] = '';
        }

        if (!empty($_FILES['tampak_empat']['name'])) {

            $file_extTampakEmpat = pathinfo($_FILES['tampak_empat']['name'], PATHINFO_EXTENSION);

            $fileNameTampakEmpat = strtotime(date('Y-m-d H:i:s')) . '_tampakEmpat.' . $file_extTampakEmpat;

            $this->uploadImagePrecheckEmpat($fileNameTampakEmpat, $no_polisi);

            $image['tampak_empat'] = $fileNameTampakEmpat;
        } else {

            $image['tampak_empat'] = '';
        }

        $image['created_at'] = date('Y-m-d H:i:s');

        $image['id_booking'] = $idBooking;

        $image['type_booking'] = 'datang';

        $image['created_by'] = $this->session->userdata('id');

        $data = array_merge($_POST, $image);

        //insert data
//        if ($_POST['id_users'] != '') {

        unset($data['nama_customer'], $data['email'], $data['id_users'], $data['no_polisi'], $data['size'], $data['price'], $data['price_id'], $data['merk'], $data['type'], $data['jenis_pekerjaan'], $data['car_color']);

        if ($this->input->post('type_booking') == 'datang') {

            $sql = $this->M_Precheck->insert($data);
        } else {

            $sql = $this->M_Precheck->updatePrecheckByIdbooking($data, $idBooking);
        }

        if ($sql == 1) {

            $this->session->set_flashdata('status', 'success');

            $this->session->set_flashdata('message', 'Data Pre Check berhasil ditambahkan');

            return redirect('precheck');
        } else {

            $this->session->set_flashdata('status', 'danger');

            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');

            return redirect('precheck');
        }

//        } else {
//            echo 'ini belum jadi';
//            exit();
//        }
    }

    public function detailPrecheck() {
        $company_id = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';
        $id_precheck = $this->uri->segment(3);
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->data['data_precheck'] = $this->M_Precheck->get_data_precheck_by_id($id_precheck, $company_id);
        $this->data['list_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['list_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['list_type'] = $this->M_Datamaster->getAllDataType();

        foreach ($this->data['data_precheck'] as $row):
            $this->data['tampak_satu'] = $row->tampak_satu;
            $this->data['tampak_dua'] = $row->tampak_dua;
            $this->data['tampak_tiga'] = $row->tampak_tiga;
            $this->data['tampak_empat'] = $row->tampak_empat;
        endforeach;
        $this->data['action'] = 'approve';
        $this->load->view('precheck_detail', $this->data);
    }

    public function detailPrecheckOnline() {
        $company_id = $this->session->userdata('company_id');
        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';
        $id_precheck = $this->uri->segment(3);
        $this->data['idMenuBack'] = $this->uri->segment(4);
        $this->data['data_precheck'] = $this->M_Precheck->get_data_precheck_by_id($id_precheck, $company_id);
        $this->data['list_service'] = $this->M_Datamaster->getAllDataService();
        $this->data['list_brand'] = $this->M_Datamaster->getAllDataBrand();
        $this->data['list_type'] = $this->M_Datamaster->getAllDataType();

        foreach ($this->data['data_precheck'] as $row):
            $this->data['tampak_satu'] = $row->tampak_satu;
            $this->data['tampak_dua'] = $row->tampak_dua;
            $this->data['tampak_tiga'] = $row->tampak_tiga;
            $this->data['tampak_empat'] = $row->tampak_empat;
        endforeach;

        $this->data['action'] = 'approve';

        $this->load->view('precheck_detail_online', $this->data);
    }

    public function seePrecheck() {

        $company_id = $this->session->userdata('company_id');

        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';

        $id_precheck = $this->uri->segment(3);

//        echo $this->uri->segment(4);
//        exit();

        $this->data['idMenuBack'] = $this->uri->segment(4);

        $this->data['data_precheck'] = $this->M_Precheck->get_data_precheck_by_id($id_precheck, $company_id);

        foreach ($this->data['data_precheck'] as $row):

            $this->data['tampak_satu'] = $row->tampak_satu;

            $this->data['tampak_dua'] = $row->tampak_dua;

            $this->data['tampak_tiga'] = $row->tampak_tiga;

            $this->data['tampak_empat'] = $row->tampak_empat;

        endforeach;

        $this->data['action'] = 'detail';

        $this->load->view('precheck_detail', $this->data);
    }

    public function approved() {

        $id_precheck = $this->input->post('id_precheck');

        $email_customer = $this->input->post('email_customer');

        $data = array(
            'status' => $this->input->post('status'),
            'approved_by' => $this->session->userdata('id')
        );
        $updated = $this->M_Precheck->updated($data, $id_precheck);

        $idBooking = $this->M_Precheck->getIdBookingByIdPrecheck($id_precheck);

        //di sini awalnya bikin pdf dan kirim email

        $dataBooking = array(
            'status' => 2,
            'updated_at' => date('Y-m-d H:i:s')
        );

        $updatedBooking = $this->M_Precheck->updatedBooking($dataBooking, $idBooking);

        if ($updated == 1 && $updatedBooking == 1) {

            $this->session->set_flashdata('status', 'success');

            $this->session->set_flashdata('message', 'Data Precheck sudah diapprove');

            return redirect('precheck');
        } else {

            $this->session->set_flashdata('status', 'danger');

            $this->session->set_flashdata('message', 'Terjadi kesalahan');

            return redirect('precheck');
        }
    }

    public function edit() {

        $company_id = $this->session->userdata('company_id');

        $this->data['menuActiveParent'] = 'precheck';
        $this->data['menuActive'] = 'precheck_index';

        $id_precheck = $this->uri->segment(3);

        $this->data['data_precheck'] = $this->M_Precheck->get_data_precheck_by_id($id_precheck, $company_id);

        $this->data['action'] = 'edit';

        $this->load->view('precheck_edit', $this->data);
    }

    public function updated() {
        $id = $this->input->post('id_precheck');
        $no_polisi = str_replace(' ', '-', $this->input->post('no_polisi'));


        $array_image = array();

        if (!empty($_FILES['tampak_satu']['name'])) {
            $file_ext = pathinfo($_FILES['tampak_satu']['name'], PATHINFO_EXTENSION);
            $fileName = strtotime(date('Y-m-d H:i:s')) . '_tampakSatu.' . $file_ext;
            $this->uploadImagePrecheck($fileName, $no_polisi, $id, 'update');
            $image['tampak_satu'] = $fileName;
        }

        if (!empty($_FILES['tampak_dua']['name'])) {
            $file_extTampakDua = pathinfo($_FILES['tampak_dua']['name'], PATHINFO_EXTENSION);
            $fileNameTampakDua = strtotime(date('Y-m-d H:i:s')) . '_tampakDua.' . $file_extTampakDua;
            $this->uploadImagePrecheckDua($fileNameTampakDua, $no_polisi);
            $image['tampak_dua'] = $fileNameTampakDua;
        }

        if (!empty($_FILES['tampak_tiga']['name'])) {
            $file_extTampakTiga = pathinfo($_FILES['tampak_tiga']['name'], PATHINFO_EXTENSION);
            $fileNameTampakTiga = strtotime(date('Y-m-d H:i:s')) . '_tampakTiga.' . $file_extTampakTiga;
            $this->uploadImagePrecheckTiga($fileNameTampakTiga, $no_polisi);
            $image['tampak_tiga'] = $fileNameTampakTiga;
        }

        if (!empty($_FILES['tampak_empat']['name'])) {
            $file_extTampakEmpat = pathinfo($_FILES['tampak_empat']['name'], PATHINFO_EXTENSION);
            $fileNameTampakEmpat = strtotime(date('Y-m-d H:i:s')) . '_tampakEmpat.' . $file_extTampakEmpat;
            $this->uploadImagePrecheckEmpat($fileNameTampakEmpat, $no_polisi);
            $image['tampak_empat'] = $fileNameTampakEmpat;
        }

        $image['updated_at'] = date('Y-m-d H:i:s');

        $data = array_merge($_POST, $image);
        unset($data['id_precheck'], $data['no_polisi'], $data['merk'], $data['type'], $data['jenis_pekerjaan'], $data['car_color']);

        $update = $this->M_Precheck->updated($data, $id);
        if ($update == 1) {
            $this->session->set_flashdata('status', 'success');
            $this->session->set_flashdata('message', 'Data Pre Check berhasil diubah');
            return redirect('precheck/detailPrecheck/' . $id);
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan dicoba beberapa saat lagi!');
            return redirect('Precheck/edit/' . $id);
        }
    }

    function uploadImagePrecheck($fileName, $noPolisi, $id, $type_booking) {
        $config['upload_path'] = './assets/images/precheck/' . $noPolisi . '/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['max_size'] = 10240;
        //        $config['max_width'] = 1024;
        //        $config['max_height'] = 768;

        $config['file_name'] = $fileName;

        $this->load->library('upload');

        $this->upload->initialize($config);

        if ($this->upload->do_upload('tampak_satu')) {
            $this->upload->data();
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            if ($type_booking == 'datang') {
                if ($id != null) {
                    return redirect('precheck/edit/' . $id);
                } else {
                    return redirect('precheck/add');
                }
            } else {
                return redirect('precheck/detailPrecheckOnline/' . $id . '/1');
            }
        }
    }

    function uploadImagePrecheckDua($fileNameTampakDua, $noPolisi, $id) {

        $config['upload_path'] = './assets/images/precheck/' . $noPolisi . '/';

        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['max_size'] = 10240;

        //        $config['max_width'] = 1024;
        //        $config['max_height'] = 768;

        $config['file_name'] = $fileNameTampakDua;



        $this->load->library('upload');



        $this->upload->initialize($config);

        if ($this->upload->do_upload('tampak_dua')) {

            $this->upload->data();
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            if ($id != null) {
                return redirect('precheck/edit/' . $id);
            } else {
                return redirect('precheck/add');
            }
        }
    }

    function uploadImagePrecheckTiga($fileNameTampakTiga, $noPolisi, $id) {

        $config['upload_path'] = './assets/images/precheck/' . $noPolisi . '/';

        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['max_size'] = 10240;

        //        $config['max_width'] = 1024;
        //        $config['max_height'] = 768;

        $config['file_name'] = $fileNameTampakTiga;

        $this->load->library('upload');



        $this->upload->initialize($config);

        if ($this->upload->do_upload('tampak_tiga')) {

            $this->upload->data();
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            if ($id != null) {
                return redirect('precheck/edit/' . $id);
            } else {
                return redirect('precheck/add');
            }
        }
    }

    function uploadImagePrecheckEmpat($fileNameTampakEmpat, $noPolisi, $id) {

        $config['upload_path'] = './assets/images/precheck/' . $noPolisi . '/';

        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $config['max_size'] = 10240;

        //        $config['max_width'] = 1024;
        //        $config['max_height'] = 768;

        $config['file_name'] = $fileNameTampakEmpat;

        $this->load->library('upload');



        $this->upload->initialize($config);

        if ($this->upload->do_upload('tampak_empat')) {

            $this->upload->data();
        } else {
            $this->session->set_flashdata('status', 'danger');
            $this->session->set_flashdata('message', $this->upload->display_errors());
            if ($id != null) {
                return redirect('precheck/edit/' . $id);
            } else {
                return redirect('precheck/add');
            }
        }
    }

    public function saveImageSignInspektor() {

        $result = array();

        $id_precheck = $_POST['id_precheck'];

        $no_polisi = str_replace(' ', '-', $_POST['no_polisi']);

        $imagedata = base64_decode($_POST['img_data']);

        $filename = strtotime(date('Y-m-d H:i:s')) . '_signInspektor.png';

        $path = './assets/images/precheck/' . $no_polisi . '/';

        //Location to where you want to created sign image

        $file_name = $path . $filename;

        file_put_contents($file_name, $imagedata);

        $result['status'] = 1;

        $result['file_name'] = $file_name;

        $data['sign_inspektor'] = $filename;

        $this->M_Precheck->updated($data, $id_precheck);

        echo json_encode($result);
    }

    public function saveImageSignCustomer() {

        $result = array();

        $id_precheck = $_POST['id_precheck'];

        $no_polisi = str_replace(' ', '-', $_POST['no_polisi']);

        $imagedata = base64_decode($_POST['img_data']);

        $filename = strtotime(date('Y-m-d H:i:s')) . '_signCustomer.png';

        $path = './assets/images/precheck/' . $no_polisi . '/';

        //Location to where you want to created sign image

        $file_name = $path . $filename;

        file_put_contents($file_name, $imagedata);

        $result['status'] = 1;

        $result['file_name'] = $file_name;

        $data['sign_customer'] = $filename;

        $this->M_Precheck->updated($data, $id_precheck);

        echo json_encode($result);
    }

    public function saveImageSignAdmin() {

        $result = array();

        $id_precheck = $_POST['id_precheck'];

        $no_polisi = str_replace(' ', '-', $_POST['no_polisi']);

        $imagedata = base64_decode($_POST['img_data']);

        $filename = strtotime(date('Y-m-d H:i:s')) . '_signAdmin.png';

        $path = './assets/images/precheck/' . $no_polisi . '/';

        //Location to where you want to created sign image

        $file_name = $path . $filename;

        file_put_contents($file_name, $imagedata);

        $result['status'] = 1;

        $result['file_name'] = $file_name;

        $data['sign_admin'] = $filename;

        $this->M_Precheck->updated($data, $id_precheck);

        echo json_encode($result);
    }

//        ini bagian menu data precheck



    public function dataPrecheck() {

        $company_id = $this->session->userdata('company_id');

        $this->data['list_precheck'] = $this->M_Precheck->get_list_all_data_precheck($company_id);
       
        $this->data['menuActiveParent'] = '';

        $this->data['menuActive'] = 'precheck_dataprecheck';

        $this->data['cari'] = 0;

        $this->data['idMenuBack'] = $this->uri->segment(4);

        $this->load->view('list_all_data_precheck', $this->data);
    }

    public function searchDataPrecheck() {

        $company_id = $this->session->userdata('company_id');

        if ($this->input->post('precheckDate') == '') {

            $precheckDate = '';

            $tanggalDipillih = '';
        } else {

            $precheckDate = date('Y-m-d', strtotime($this->input->post('precheckDate')));

            $tanggalDipillih = date('d-m-Y', strtotime($this->input->post('precheckDate')));

            ;
        }

        $status = $this->input->post('status');

        $this->data['list_precheck'] = $this->M_Precheck->get_list_all_data_precheck_byDatePrecheck($precheckDate, $status, $company_id);

        $this->data['menuActive'] = 'precheck_dataprecheck';

        $this->data['precheckDate'] = $tanggalDipillih;

        $this->data['status'] = $this->input->post('status');

        $this->data['cari'] = 1;

        $this->data['idMenuBack'] = $this->uri->segment(4);

        $this->load->view('list_all_data_precheck', $this->data);
    }

    public function cetakInoviceDpPdf($idbooking, $filename) {

        $data['data'] = $this->M_Precheck->get_data_transaction_by_id_booking($idbooking);
//        foreach ($data['data'] as $row):
//            //base 64 image
//            $path = base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_admin);
//            $type = pathinfo($path, PATHINFO_EXTENSION);
//            $datas = file_get_contents($path);
//            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($datas);
//            $data['data'][0]->sign_admin_64 = $base64;
//        endforeach;
        //cetak dan save pdfnya
        #--CREATE PDF
        $this->load->library('pdf');
        $data['tanggal'] = date('d M Y');
        $html = $this->load->view('pdf_invoiceDp', $data, true);
        $this->pdf->createPDF($html, $filename);

        //update invoice di db
        $dataBooking = array(
            'invoice_dp' => $filename
        );
        $this->M_Precheck->updatedBooking($dataBooking, $idbooking);
        //akhir update data
    }

}
