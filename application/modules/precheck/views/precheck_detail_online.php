<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Precheck</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            #btnSaveSign,
            #btnSaveSign2,
            #btnSaveSign3 {
                color: #fff;
                background: #f99a0b;
                padding: 5px;
                border: none;
                border-radius: 5px;
                font-size: 20px;
                margin-top: 10px;
                /* margin-left: 25px; */
            }

            #btnClearSign,
            #btnClearSign2,
            #btnClearSign3 {
                color: #fff;
                background: blue;
                padding: 5px;
                border: none;
                border-radius: 5px;
                font-size: 20px;
                margin-top: 10px;
                /* margin-left: 25px; */
            }

            #signArea {
                width: 304px;
                /* margin: 50px auto; */
            }

            .sign-container {
                width: 60%;
                margin: auto;
            }

            .sign-preview,
            .sign-preview2,
            .sign-preview3 {
                width: 304px;
                height: 90px;
                border: solid 0px #CFCFCF;
                margin: 10px 5px;
            }

            .tag-ingo {
                font-family: cursive;
                font-size: 12px;
                text-align: left;
                font-style: oblique;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            #imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            /*#imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 :hover {opacity: 0.7;}*/

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image */
            #caption, #caption2, #caption3, #caption4 {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content, #caption, #caption2, #caption3, #caption4 {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close, .close2, .close3, .close4 {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close, .close2, .close3, .close4:hover,
            .close, .close2, .close3, .close4:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }
        </style>
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_satu').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLDua(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_dua').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLTiga(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_tiga').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function readURLEmpat(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#tampak_empat').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <form method="post" action="<?= base_url('precheck/insert') ?>" class="form-horizontal" id="sample-form" enctype="multipart/form-data">
                    
                    <input type="hidden" name="type_booking" value="online"/>
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Home</a>
                            </li>

                            <li>
                                <a href="<?= base_url('precheck') ?>">Precheck</a>
                            </li>

                            <li>
                                <a href="#">Add Precheck</a>
                            </li>
                        </ul><!-- /.breadcrumb -->
                    </div>

                    <div class="page-content">
                        <?php if ($this->session->flashdata('message')) : ?>
                            <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="page-header" style="text-align: center;">
                            <h1 style="color: black;">
                                <b>PRE CHECK INSPECTION</b>
                            </h1>

                        </div>
                        <div class="widget-box">
                            <?php foreach ($data_precheck as $row) : ?>
                            <input type="hidden" name="id_booking" value="<?= $row->id_booking?>"/>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div id="fuelux-wizard-container">
                                            <div class="step-content pos-rel">
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Nama Customer</label>
                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <?php $name_full = $row->name . '(' . $row->email . ')'; ?>
                                                            <input type="text" name="nama_customer" class="width-100" readonly value="<?= $name_full; ?>" />
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">No Polisi</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" id="no_polisi" name="no_polisi" class="width-100" value="<?= $row->no_polisi ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Merk/Type</label>

                                                    <div class="col-sm-2">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="merk" class="width-100" value="<?= $row->merk_mobil ?>" readonly />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="type" class="width-100" value="<?= $row->type_mobil ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Warna Mobil</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $row->car_color ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Jenis Pekerjaan</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="jenis_pekerjaan" class="width-100" value="<?= $row->nama_servis ?>" readonly />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">Harga</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" readonly id="price" name="price" class="width-100" value="Rp. <?= number_format($row->price)?>" />
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="input" class="col-sm-2 no-padding-right">DP</label>

                                                    <div class="col-sm-4">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" readonly id="price" name="price" class="width-100" value="Rp. <?= number_format($row->dp)?>" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-main -->
                                <?php endforeach; ?>
                            </div><!-- /.widget-body -->
                        </div>
                    </div>

                    <div class="page-content">

                        <div class="page-header" style="text-align: center;">
                            <h1 style="color: black;">
                                <b>KELENGKAPAN</b>
                            </h1>
                        </div><!-- /.page-header -->
                        <div class="widget-box">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="fuelux-wizard-container">
                                        <div class="step-content pos-rel">
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">&nbsp;</label>

                                                <div class="col-sm-2" style="text-align: center;">
                                                    <b>Masuk</b>
                                                </div>
                                                <div class="col-sm-2" style="text-align: center;">
                                                    <b>Keluar</b>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">&nbsp;</label>

                                                <div class="col-sm-2" style="text-align: center;">
                                                    <b>Masuk</b>
                                                </div>
                                                <div class="col-sm-2" style="text-align: center;">
                                                    <b>Keluar</b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Ban Cadangan</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="ban_cadangan_masuk" id="ban_cadangan_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="ban_cadangan_keluar">
                                                    <select class="form-control" name="ban_cadangan_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Air Conditioning & Blower</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" id="airConditioning_masuk" name="airConditioning_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="airConditioning_keluar">
                                                    <select class="form-control" name="airConditioning_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Baut Derek</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="bautDerek_masuk" id="bautDerek_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="bautDerek_keluar">
                                                    <select class="form-control" name="bautDerek_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Antena</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="antena_masuk" id="antena_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="antena_keluar">
                                                    <select class="form-control" name="antena_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Buku Service & Buku Perawatan</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="bukuService_masuk" id="bukuService_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="bukuService_keluar">
                                                    <select class="form-control" name="bukuService_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Foldback Mirror Electric</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="floodback_masuk" id="floodback_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="floodback_keluar">
                                                    <select class="form-control" name="floodback_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">PIN Radio</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="pinRadion_masuk" id="pinRadion_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="pinRadion_keluar">
                                                    <select class="form-control" name="pinRadio_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Air Radiator</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="airRadiator_masuk" id="airRadiator_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="airRadiator_keluar">
                                                    <select class="form-control" name="airRadiator_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Dongkrak</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="dongkrak_masuk" id="dongkrak_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="dongkrak_keluar">
                                                    <select class="form-control" name="dongkrak_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Ketinggian Brake & Cluth Fluit</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="ketinggianBrake_masuk" id="ketinggianBrake_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="ketinggianBrake_keluar">
                                                    <select class="form-control" name="ketinggianBrake_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Dop/Tutup Roda</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="dopTutupRoda_masuk" id="dopTutupRoda_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="dopTutupRoda_keluar">
                                                    <select class="form-control" name="dopTutupRoda_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Ketinggian Engine Oil</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="ketinggianEngine_masuk" id="ketinggianEngine_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="ketinggianEngine_keluar">
                                                    <select class="form-control" name="ketinggianEngine_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Tas P3K</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="tasP3k_masuk" id="tasP3k_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="tasP3k_keluar">
                                                    <select class="form-control" name="tasP3k_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Ketinggian PowerSteering Fluit</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="ketinggianPowerSteering_masuk" id="ketinggianPowerSteering_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="ketinggianPowerSteering_keluar">
                                                    <select class="form-control" name="ketinggianPowerSteering_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Dudukan Plat Nomor</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="dudukanPlat_masuk" id="dudukanPlat_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="dudukanPlat_keluar">
                                                    <select class="form-control" name="dudukanPlat_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Instrumen Panel</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuInstrumen_masuk" id="lampuInstrumen_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuInstrumen_keluar">
                                                    <select class="form-control" name="lampuInstrumen_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>
                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Jok Depan & Belakang</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="jokDepanBelakang_masuk" id="jokDepanBelakang_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="jokDepanBelakang_keluar">
                                                    <select class="form-control" name="jokDepanBelakang_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Belok & Darurat</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuBelokDarurat_masuk" id="lampuBelokDarurat_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuBelokDarurat_keluar">
                                                    <select class="form-control" name="lampuBelokDarurat_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Kaca Spion Luar Lt l/RH</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="kacaSpionLuar_masuk" id="kacaSpionLuar_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="kacaSpionLuar_keluar">
                                                    <select class="form-control" name="kacaSpionLuar_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Dalam</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuDalam_masuk" id="lampuDalam_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuDalam_keluar">
                                                    <select class="form-control" name="lampuDalam_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Kaca Spion Dalam</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="kacaSpionDalam_masuk" id="kacaSpionDalam_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="kacaSpionDalam_keluar">
                                                    <select class="form-control" name="kacaSpionDalam_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Dalam & Kecil</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuDalamKecil_masuk" id="lampuDalamKecil_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuDalamKecil_keluar">
                                                    <select class="form-control" name="lampuDalamKecil_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Kunci Remot & Kontak</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="kunciRemotKontak_masuk" id="kunciRemotKontak_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="kunciRemotKontak_keluar">
                                                    <select class="form-control" name="kunciRemotKontak_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Kabut</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuKabut_masuk" id="lampuKabut_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuKabut_keluar">
                                                    <select class="form-control" name="lampuKabut_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Penutup Kaca Belakang</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="penutupKacaBelakang_masuk" id="penutupKacaBelakang_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="penutupKacaBelakang_keluar">
                                                    <select class="form-control" name="penutupKacaBelakang_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Lampu Rem Parkir & Mundur</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="lampuRemParkirMundur_masuk" id="lampuRemParkirMundur_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="lampuRemParkirMundur_keluar">
                                                    <select class="form-control" name="lampuRemParkirMundur_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Penyulut Rokok</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="penyulutRoko_masuk" id="penyulutRoko_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="penyulutRoko_keluar">
                                                    <select class="form-control" name="penyulutRoko_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Power Window</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="powerWindow_masuk" id="powerWindow_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="powerWindow_keluar">
                                                    <select class="form-control" name="powerWindow_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Sayap Bumper Depan</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="sayapBumperDepan_masuk" id="sayapBumperDepan_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="sayapBumperDepan_keluar">
                                                    <select class="form-control" name="sayapBumperDepan_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Radio/Tape</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="radioTape_masuk" id="radioTape_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="radioTape_keluar">
                                                    <select class="form-control" name="radioTape_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Segitiga Pengaman</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="segitigaPengaman_masuk" id="segitigaPengaman_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="segitigaPengaman_keluar">
                                                    <select class="form-control" name="segitigaPengaman_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">CD Player & Magazine</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="cdPlayerMagazine_masuk" id="cdPlayerMagazine_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="cdPlayerMagazine_keluar">
                                                    <select class="form-control" name="cdPlayerMagazine_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Sensor Parkir Mundur</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="sensorParkirMundur_masuk" id="sensorParkirMundur_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="sensorParkirMundur_keluar">
                                                    <select class="form-control" name="sensorParkirMundur_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Central Lock</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="centralLock_masuk" id="centralLock_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="centralLock_keluar">
                                                    <select class="form-control" name="centralLock_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Set Karpet</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="setKarpet_masuk" id="setKarpet_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="setKarpet_keluar">
                                                    <select class="form-control" name="setKarpet_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Accu</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="accu_masuk" id="accu_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="accu_keluar">
                                                    <select class="form-control" name="accu_keluar">
                                                        <option value="v">Ada & Berfungsi</option>													<option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Payung</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="payung_masuk" id="payung_masuk">
                                                        <option value="v">Ada & Berfungsi</option>													<option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="payung_keluar">
                                                    <select class="form-control" name="payung_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Telephone</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="telephone_masuk" id="telephone_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="telephone_keluar">
                                                    <select class="form-control" name="telephone_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">SNTK</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="stnk_masuk" id="stnk_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="stnk_keluar">
                                                    <select class="form-control" name="stnk_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>

                                                <label for="input" class="col-sm-2 no-padding-right">Wiper / Penyemprot Kaca</label>

                                                <div class="col-sm-2">
                                                    <select class="form-control" name="wiperPenyemprotKaca_masuk" id="wiperPenyemprotKaca_masuk">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-2" id="wiperPenyemprotKaca_keluar">
                                                    <select class="form-control" name="wiperPenyemprotKaca_keluar">
                                                        <option value="v">Ada & Berfungsi</option>
                                                        <option value="x">Tidak Ada</option>

                                                        <option value="r">Ada & Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                &nbsp;
                                            </div>
                                            <div class="form-group">
                                                <label for="input" class="col-sm-2 no-padding-right">Lain-lain</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" name="lain_lain" style="width: 100%; height: 100px;"><?= $row->lain_lain ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                &nbsp;
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12" style="text-align: center">
                                                    <label for="input" class="col-sm-12 no-padding-right" style="font-size: 20px;"><b>Photo Exterior</b></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 1
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 3
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_satu != '') : ?>
                                                            <img id="imageTampak1"  src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_satu) ?>" id="tampak_satu" style="width:65%;height:200px;" />
                                                            <div id="caption"></div>
                                                        <?php else : ?>
                                                            <img src="<?= base_url('assets/images/NoImage.gif') ?>" id="tampak_satu" style="width:50%;height:200px;" />
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_tiga != '') : ?>
                                                            <img id="imageTampak3" src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_tiga) ?>" id="tampak_tiga" style="width:65%;height:200px;" />
                                                            <div id="caption3"></div>
                                                        <?php else : ?>
                                                            <img src="<?= base_url('assets/images/NoImage.gif') ?>" id="tampak_tiga" style="width:50%;height:200px;" />
                                                        <?php endif; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2" style="text-align: center">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-4" style="text-align: center">
                                                        <input type='file' name="tampak_satu" id="tampak_satu" onchange="readURL(this);" />
                                                    </div>
                                                    <div class="col-sm-2" style="text-align: center">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-4" style="text-align: center">
                                                        <input type='file' name="tampak_tiga" id="tampak_tiga" onchange="readURLTiga(this);" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_satu" style="width: 100%; height: 100px;"><?= $row->keterangan_tampak_satu ?></textarea>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_tiga" style="width: 100%; height: 100px;"><?= $row->keterangan_tampak_tiga ?></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 2
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        Tampak 4
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_dua != '') : ?>
                                                            <img id="imageTampak2" src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_dua) ?>" id="tampak_dua" style="width:65%;height:200px;" />
                                                            <div id="caption2"></div>
                                                        <?php else : ?>
                                                            <img src="<?= base_url('assets/images/NoImage.gif') ?>" id="tampak_dua" style="width:50%;height:200px;" />
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <?php if ($row->tampak_empat != '') : ?>
                                                            <img id="imageTampak4" src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->tampak_empat) ?>" id="tampak_empat" style="width:65%;height:200px;" />
                                                            <div id="caption4"></div>
                                                        <?php else : ?>
                                                            <img src="<?= base_url('assets/images/NoImage.gif') ?>" id="tampak_empat" style="width:50%;height:200px;" />
                                                        <?php endif; ?>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2" style="text-align: center">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-4" style="text-align: center">
                                                        <input type='file' name="tampak_dua" id="tampak_dua" onchange="readURLDua(this);" />
                                                    </div>
                                                    <div class="col-sm-2" style="text-align: center">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-sm-4" style="text-align: center">
                                                        <input type='file' name="tampak_empat" id="tampak_empat" onchange="readURLEmpat(this);" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center;">
                                                        Keterangan
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_dua" style="width: 100%; height: 100px;"><?= $row->keterangan_tampak_dua ?></textarea>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: center">
                                                        <textarea class="form-control" name="keterangan_tampak_empat" style="width: 100%; height: 100px;"><?= $row->keterangan_tampak_empat ?></textarea>
                                                    </div>
                                                </div>

                                                <!-- form tanda tangan-->
<!--                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_inspektor == null || $row->sign_inspektor == '') : ?>
                                                            <div id="signArea">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Inspektor,</h2>
                                                                <div class="sig sigWrapper" id="position" style="height:auto;">

                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>

                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Inspektor,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_inspektor) ?>" class="sign-preview" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>



                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_customer == null || $row->sign_customer == '') : ?>
                                                            <div id="signArea2">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Customer,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <div id="position2"></div>
                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad2" id="sign-pad2" width="300" height="100"></canvas>


                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Customer,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_customer) ?>" class="sign-preview2" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_admin == null || $row->sign_admin == '') : ?>
                                                            <div id="signArea3">
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Admin,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <div id="position3"></div>
                                                                    <div class="typed"></div>
                                                                    <canvas class="sign-pad3" id="sign-pad3" width="300" height="100"></canvas>


                                                                </div>
                                                            </div>
                                                        <?php else : ?>
                                                            <div>
                                                                <h2 style="font-family: Arial, Helvetica, sans-serif;">Admin,</h2>
                                                                <div class="sig sigWrapper" style="height:auto;">
                                                                    <img src="<?= base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_admin) ?>" class="sign-preview3" />
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_inspektor == null || $row->sign_inspektor == '') : ?>
                                                            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                                <button id="btnSaveSign">Save Signature</button>
                                                                <button id="btnClearSign">Hapus</button>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_customer == null || $row->sign_customer == '') : ?>
                                                            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                                <button id="btnSaveSign2">Save Signature</button>
                                                                <button id="btnClearSign2">Hapus</button>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php if ($row->sign_admin == null || $row->sign_admin == '') : ?>
                                                            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                                                                <button id="btnSaveSign3">Save Signature</button>
                                                                <button id="btnClearSign3">Hapus</button>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>-->
                                                <input type="hidden" id="id_precheck" value="<?= $row->id; ?>" />
                                                <br>
                                                <?php if ($action === 'approve') : ?>
                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                                                        <div class="form-group">
                                                            <div class="col-sm-12" style="text-align: center">
                                                                <input type="hidden" name="id_precheck" value="<?= $row->id; ?>" />
                                                                <input type="hidden" name="status" value="1" />

                                                                <button type="submit" class="btn btn-primary btn-block mt-3" style="border-radius: 8px;" onclick="return confirm('Setuju dengan data pre check nya?');"><span class="fa fa-check"></span> Simpan</button>

                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 2) : ?>
                                                        <?php if ($row->sign_inspektor == null || $row->sign_customer == null) : ?>
<!--                                                            <div class="form-group">
                                                                <div class="col-sm-12" style="text-align: center">
                                                                    <a href="</?= base_url('precheck/edit/' . $row->id) ?>" class="btn btn-success btn-block mt-3" style="border-radius: 8px;"><span class="fa fa-edit"></span> Final Inspek</a>
                                                                </div>
                                                            </div>-->
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <div class="form-group">
                                                    <div class="col-sm-12" style="text-align: center">
                                                        <?php if ($idMenuBack == 1): ?>
                                                            <a href="<?= base_url('precheck') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;"> <span class="fa fa-backward"></span> Kembali</a>
                                                        <?php elseif ($idMenuBack == 2): ?>
                                                            <a href="<?= base_url('status/proses') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;"> <span class="fa fa-backward"></span> Kembali</a>
                                                        <?php elseif ($idMenuBack == 3): ?>
                                                            <a href="<?= base_url('precheck/dataPrecheck') ?>" class="btn btn-danger btn-block mt-3" style="border-radius: 8px;"> <span class="fa fa-backward"></span> Kembali</a>                                                            
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div>
                    </div>
                </form>
            </div><!-- /.main-content -->


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
                                                        if ('ontouchstart' in document.documentElement)
                                                            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.buttons.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.flash.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.html5.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.print.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.colVis.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.select.min.js') ?>"></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?>"></script>
        <script>
                                                        jQuery(function ($) {
                                                            if (!ace.vars['touch']) {
                                                                $('.chosen-select').chosen({
                                                                    allow_single_deselect: true
                                                                });
                                                                //resize the chosen on window resize

                                                                $(window)
                                                                        .off('resize.chosen')
                                                                        .on('resize.chosen', function () {
                                                                            $('.chosen-select').each(function () {
                                                                                var $this = $(this);
                                                                                $this.next().css({
                                                                                    'width': $this.parent().width()
                                                                                });
                                                                            })
                                                                        }).trigger('resize.chosen');
                                                                //resize chosen on sidebar collapse/expand
                                                                $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
                                                                    if (event_name != 'sidebar_collapsed')
                                                                        return;
                                                                    $('.chosen-select').each(function () {
                                                                        var $this = $(this);
                                                                        $this.next().css({
                                                                            'width': $this.parent().width()
                                                                        });
                                                                    })
                                                                });
                                                            }
                                                        });

                                                        $('a').click(function (e) {
                                                            if ($(this).attr('href') === '#user_yet') {
                                                                e.preventDefault();
                                                                document.getElementById("user_yet").style.display = 'block';
                                                                document.getElementById("user_new").style.display = 'none';
                                                                document.getElementById("id_users").required = true;
                                                                document.getElementById("nama_customer").required = false;
                                                                document.getElementById("email").required = false;
                                                                document.getElementById("nama_customer").value = '';
                                                                document.getElementById("email").value = '';
                                                            } else if ($(this).attr('href') === '#user_new') {
                                                                e.preventDefault();
                                                                document.getElementById("user_yet").style.display = 'none';
                                                                document.getElementById("user_new").style.display = 'block';
                                                                document.getElementById("id_users").required = false;
                                                                document.getElementById("id_users").value = '';
                                                                document.getElementById("nama_customer").required = true;
                                                                document.getElementById("email").required = true;
                                                            }
                                                        });
        </script>
        <script>
            $(document).ready(function () {
                $('#ban_cadangan_masuk').change(function () {
                    var ban_cadangan_masuk = $('#ban_cadangan_masuk').val();
                    $("div#ban_cadangan_keluar select").val(ban_cadangan_masuk);
                });

                $('#airConditioning_masuk').change(function () {
                    var airConditioning_masuk = $('#airConditioning_masuk').val();
                    $("div#airConditioning_keluar select").val(airConditioning_masuk);
                });

                $('#bautDerek_masuk').change(function () {
                    var bautDerek_masuk = $('#bautDerek_masuk').val();
                    $("div#bautDerek_keluar select").val(bautDerek_masuk);
                });

                $('#antena_masuk').change(function () {
                    var antena_masuk = $('#antena_masuk').val();
                    $("div#antena_keluar select").val(antena_masuk);
                });

                $('#bukuService_masuk').change(function () {
                    var bukuService_masuk = $('#bukuService_masuk').val();
                    $("div#bukuService_keluar select").val(bukuService_masuk);
                });

                $('#floodback_masuk').change(function () {
                    var floodback_masuk = $('#floodback_masuk').val();
                    $("div#floodback_keluar select").val(floodback_masuk);
                });

                $('#pinRadion_masuk').change(function () {
                    var pinRadion_masuk = $('#pinRadion_masuk').val();
                    $("div#pinRadion_keluar select").val(pinRadion_masuk);
                });

                $('#airRadiator_masuk').change(function () {
                    var airRadiator_masuk = $('#airRadiator_masuk').val();
                    $("div#airRadiator_keluar select").val(airRadiator_masuk);
                });

                $('#dongkrak_masuk').change(function () {
                    var dongkrak_masuk = $('#dongkrak_masuk').val();
                    $("div#dongkrak_keluar select").val(dongkrak_masuk);
                });

                $('#ketinggianBrake_masuk').change(function () {
                    var ketinggianBrake_masuk = $('#ketinggianBrake_masuk').val();
                    $("div#ketinggianBrake_keluar select").val(ketinggianBrake_masuk);
                });

                $('#dopTutupRoda_masuk').change(function () {
                    var dopTutupRoda_masuk = $('#dopTutupRoda_masuk').val();
                    $("div#dopTutupRoda_keluar select").val(dopTutupRoda_masuk);
                });

                $('#ketinggianEngine_masuk').change(function () {
                    var ketinggianEngine_masuk = $('#ketinggianEngine_masuk').val();
                    $("div#ketinggianEngine_keluar select").val(ketinggianEngine_masuk);
                });

                $('#tasP3k_masuk').change(function () {
                    var tasP3k_masuk = $('#tasP3k_masuk').val();
                    $("div#tasP3k_keluar select").val(tasP3k_masuk);
                });

                $('#ketinggianPowerSteering_masuk').change(function () {
                    var ketinggianPowerSteering_masuk = $('#ketinggianPowerSteering_masuk').val();
                    $("div#ketinggianPowerSteering_keluar select").val(ketinggianPowerSteering_masuk);
                });

                $('#dudukanPlat_masuk').change(function () {
                    var dudukanPlat_masuk = $('#dudukanPlat_masuk').val();
                    $("div#dudukanPlat_keluar select").val(dudukanPlat_masuk);
                });

                $('#lampuInstrumen_masuk').change(function () {
                    var lampuInstrumen_masuk = $('#lampuInstrumen_masuk').val();
                    $("div#lampuInstrumen_keluar select").val(lampuInstrumen_masuk);
                });

                $('#jokDepanBelakang_masuk').change(function () {
                    var jokDepanBelakang_masuk = $('#jokDepanBelakang_masuk').val();
                    $("div#jokDepanBelakang_keluar select").val(jokDepanBelakang_masuk);
                });

                $('#lampuBelokDarurat_masuk').change(function () {
                    var lampuBelokDarurat_masuk = $('#lampuBelokDarurat_masuk').val();
                    $("div#lampuBelokDarurat_keluar select").val(lampuBelokDarurat_masuk);
                });

                $('#kacaSpionLuar_masuk').change(function () {
                    var kacaSpionLuar_masuk = $('#kacaSpionLuar_masuk').val();
                    $("div#kacaSpionLuar_masuk select").val(kacaSpionLuar_masuk);
                });

                $('#lampuDalam_masuk').change(function () {
                    var lampuDalam_masuk = $('#lampuDalam_masuk').val();
                    $("div#lampuDalam_keluar select").val(lampuDalam_masuk);
                });

                $('#kacaSpionDalam_masuk').change(function () {
                    var kacaSpionDalam_masuk = $('#kacaSpionDalam_masuk').val();
                    $("div#kacaSpionDalam_keluar select").val(kacaSpionDalam_masuk);
                });

                $('#lampuDalamKecil_masuk').change(function () {
                    var lampuDalamKecil_masuk = $('#lampuDalamKecil_masuk').val();
                    $("div#lampuDalamKecil_keluar select").val(lampuDalamKecil_masuk);
                });

                $('#kunciRemotKontak_masuk').change(function () {
                    var kunciRemotKontak_masuk = $('#kunciRemotKontak_masuk').val();
                    $("div#kunciRemotKontak_keluar select").val(kunciRemotKontak_masuk);
                });

                $('#lampuKabut_masuk').change(function () {
                    var lampuKabut_masuk = $('#lampuKabut_masuk').val();
                    $("div#lampuKabut_keluar select").val(lampuKabut_masuk);
                });

                $('#penutupKacaBelakang_masuk').change(function () {
                    var penutupKacaBelakang_masuk = $('#penutupKacaBelakang_masuk').val();
                    $("div#penutupKacaBelakang_keluar select").val(penutupKacaBelakang_masuk);
                });

                $('#lampuRemParkirMundur_masuk').change(function () {
                    var lampuRemParkirMundur_masuk = $('#lampuRemParkirMundur_masuk').val();
                    $("div#lampuRemParkirMundur_keluar select").val(lampuRemParkirMundur_masuk);
                });

                $('#kacaSpionLuar_masuk').change(function () {
                    var kacaSpionLuar_masuk = $('#kacaSpionLuar_masuk').val();
                    $("div#kacaSpionLuar_keluar select").val(kacaSpionLuar_masuk);
                });

                $('#penyulutRoko_masuk').change(function () {
                    var penyulutRoko_masuk = $('#penyulutRoko_masuk').val();
                    $("div#penyulutRoko_keluar select").val(penyulutRoko_masuk);
                });

                $('#powerWindow_masuk').change(function () {
                    var powerWindow_masuk = $('#powerWindow_masuk').val();
                    $("div#powerWindow_keluar select").val(powerWindow_masuk);
                });

                $('#sayapBumperDepan_masuk').change(function () {
                    var sayapBumperDepan_masuk = $('#sayapBumperDepan_masuk').val();
                    $("div#sayapBumperDepan_keluar select").val(sayapBumperDepan_masuk);
                });

                $('#radioTape_masuk').change(function () {
                    var radioTape_masuk = $('#radioTape_masuk').val();
                    $("div#radioTape_keluar select").val(radioTape_masuk);
                });

                $('#segitigaPengaman_masuk').change(function () {
                    var segitigaPengaman_masuk = $('#segitigaPengaman_masuk').val();
                    $("div#segitigaPengaman_keluar select").val(segitigaPengaman_masuk);
                });

                $('#cdPlayerMagazine_masuk').change(function () {
                    var cdPlayerMagazine_masuk = $('#cdPlayerMagazine_masuk').val();
                    $("div#cdPlayerMagazine_keluar select").val(cdPlayerMagazine_masuk);
                });

                $('#sensorParkirMundur_masuk').change(function () {
                    var sensorParkirMundur_masuk = $('#sensorParkirMundur_masuk').val();
                    $("div#sensorParkirMundur_keluar select").val(sensorParkirMundur_masuk);
                });

                $('#centralLock_masuk').change(function () {
                    var centralLock_masuk = $('#centralLock_masuk').val();
                    $("div#centralLock_keluar select").val(centralLock_masuk);
                });

                $('#setKarpet_masuk').change(function () {
                    var setKarpet_masuk = $('#setKarpet_masuk').val();
                    $("div#setKarpet_keluar select").val(setKarpet_masuk);
                });

                $('#accu_masuk').change(function () {
                    var accu_masuk = $('#accu_masuk').val();
                    $("div#accu_keluar select").val(accu_masuk);
                });

                $('#payung_masuk').change(function () {
                    var payung_masuk = $('#payung_masuk').val();
                    $("div#payung_keluar select").val(payung_masuk);
                });

                $('#telephone_masuk').change(function () {
                    var telephone_masuk = $('#telephone_masuk').val();
                    $("div#telephone_keluar select").val(telephone_masuk);
                });

                $('#stnk_masuk').change(function () {
                    var stnk_masuk = $('#stnk_masuk').val();
                    $("div#stnk_keluar select").val(stnk_masuk);
                });

                $('#wiperPenyemprotKaca_masuk').change(function () {
                    var wiperPenyemprotKaca_masuk = $('#wiperPenyemprotKaca_masuk').val();
                    $("div#wiperPenyemprotKaca_keluar select").val(wiperPenyemprotKaca_masuk);
                });


            });
        </script>
        <script src="<?= base_url('assets/js/sign/numeric-1.2.6.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/sign/bezier.js') ?>"></script>
        <script src="<?= base_url('assets/js/sign/jquery.signaturepad.js') ?>"></script>
        <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="<?= base_url('assets/js/sign/json2.min.js') ?>"></script>
        <script>
            var id_precheck = document.getElementById("id_precheck").value;
            var no_polisi = document.getElementById("no_polisi").value;
//                                                        var canvas = document.getElementById('sign-pad');
//                                                        var signaturePad = new SignaturePad(canvas, {
//                                                            backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
//                                                        });
            $(document).ready(function () {
                $('#signArea').signaturePad({
                    clear: $('#btnClearSign'),
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signAreadisabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign").click(function (e) {
                html2canvas([document.getElementById('sign-pad')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignInspektor') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
//                                                                            $('#sign-pad').load(document.URL +  ' #sign-pad');
//                                                                            window.location.hash = '.sign-pad';
//                                                                            window.location.reload(true);
                                gototab();
                            }
                        });
                    }
                });
            });

            $(document).ready(function () {
                $('#signArea2').signaturePad({
                    clear: $('#btnClearSign2'),
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signArea2disabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign2").click(function (e) {
                html2canvas([document.getElementById('sign-pad2')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignCustomer') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
//                                                                            window.location.hash = '#sign-pad2';
//                                                                            window.location.reload();
                                gototab2();
                            }
                        });
                    }
                });
            });

            $(document).ready(function () {
                $('#signArea3').signaturePad({
                    clear: $('#btnClearSign3'),
                    drawOnly: true,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });
            $(document).ready(function () {
                $('#signArea3disabled').signaturePad({
                    drawOnly: false,
                    drawBezierCurves: true,
                    lineTop: 90
                });
            });

            $("#btnSaveSign3").click(function (e) {
                html2canvas([document.getElementById('sign-pad3')], {
                    onrendered: function (canvas) {
                        var canvas_img_data = canvas.toDataURL('image/png');
                        var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
                        // alert(no_polisi);
                        //ajax call to save image inside folder
                        $.ajax({
                            url: "<?= base_url('precheck/saveImageSignAdmin') ?>",
                            data: {
                                img_data: img_data,
                                id_precheck: id_precheck,
                                no_polisi: no_polisi
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (response) {
//                                                                            window.location.hash = '#sign-pad3';
//                                                                            window.location.reload();
                                gototab3();
                            }
                        });
                    }
                });
            });

            function gototab(reload)
            {
                window.location.hash = '#position';
                window.location.reload(true);
            }
            function gototab2(reload)
            {
                window.location.hash = '#position2';
                window.location.reload(true);
            }
            function gototab3(reload)
            {
                window.location.hash = '#position3';
                window.location.reload(true);
            }
        </script>
    </body>

</html>
