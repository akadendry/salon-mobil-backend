<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Report');
        if ($this->session->userdata('email') == NULL) {
            return redirect('login');
        }
        date_default_timezone_set("Asia/Jakarta");
    }

    public function transcation() {
        $company_id = $this->session->userdata('company_id');
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');
        $this->data['menuActiveParent'] = 'report';
        $this->data['menuActive'] = 'report_transaksi';
        $this->data['cari'] = 0;
        $status = 0;
        $this->data['transaksi'] = $this->M_Report->getAllTransaksiByIdCompany($company_id, $start_date,$end_date, $status);
        $this->data['status'] = $status;
        $this->data['total_bayar_pusat'] = 0;
        $this->load->view('transaction', $this->data);
    }
    
    public function searchDataTransaksi(){
        $company_id = $this->session->userdata('company_id');
        $start_date = $this->input->post('start_date');
        $status = $this->input->post('status');
        if($start_date != ''){
            $start_date = date('Y-m-d', strtotime($start_date));
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date('Y-m-d', strtotime($end_date));
        }
        $this->data['transaksi'] = $this->M_Report->getAllTransaksiByIdCompany($company_id, $start_date,$end_date, $status);
        $this->data['cari'] = 1;
        $this->data['menuActiveParent'] = 'report';
        $this->data['menuActive'] = 'report_transaksi';
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        $this->data['status'] = $status;
        $this->load->view('transaction', $this->data);
    }
    
    public function dp() {
        $company_id = $this->session->userdata('company_id');
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');
        $this->data['menuActiveParent'] = 'report';
        $this->data['menuActive'] = 'report_dp';
        $this->data['cari'] = 0;
        $this->data['transaksi'] = $this->M_Report->getAllDpTransaksiByIdCompany($company_id, $start_date,$end_date);
        $this->data['total_bayar_pusat'] = 0;
        $this->load->view('downpayment', $this->data);
    }
    
    public function searchDataDp(){
        $company_id = $this->session->userdata('company_id');
        $start_date = $this->input->post('start_date');
        if($start_date != ''){
            $start_date = date('Y-m-d', strtotime($start_date));
        }
        $end_date = $this->input->post('end_date');
        if($end_date != ''){
            $end_date = date('Y-m-d', strtotime($end_date));
        }
        $this->data['transaksi'] = $this->M_Report->getAllDpTransaksiByIdCompany($company_id, $start_date,$end_date);
        $this->data['cari'] = 1;
        $this->data['menuActiveParent'] = 'report';
        $this->data['menuActive'] = 'report_dp';
        $this->data['start_date'] = $this->input->post('start_date');
        $this->data['end_date'] = $this->input->post('end_date');
        $this->load->view('downpayment', $this->data);
    }
    
    public function transactionExportExcel(){
        $company_id = $this->session->userdata('company_id');
        $status = $this->input->post('status');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $this->data['transaksi'] = $this->M_Report->getAllTransaksiByIdCompany($company_id, $start_date,$end_date, $status);
        $this->data['title'] = "Report Transaksi";
        $this->load->view('cetak_transaction', $this->data);
    }
    
    public function dpExportExcel(){
        $company_id = $this->session->userdata('company_id');
        $status = $this->input->post('status');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $this->data['transaksi'] = $this->M_Report->getAllDpTransaksiByIdCompany($company_id, $start_date,$end_date);
        $this->data['title'] = "Report Downpayment";
        $this->load->view('cetak_downpayment', $this->data);
    }

}
