<?php

class M_Report extends CI_Model {

    public function getAllTransaksiByIdCompany($company_id, $start_date,$end_date, $status){
        if($start_date == '' && $end_date == ''){
            $kondisi = '';
        }
        if($start_date != '' && $end_date == ''){
            $kondisi = "AND DATE(a.book_date) >= '$start_date'";
        }
        if($start_date == '' && $end_date != ''){
            $kondisi = "AND DATE(a.book_date) <= '$end_date'";
        }
        if($start_date != '' && $end_date != ''){
            $kondisi = "AND (DATE(a.book_date) BETWEEN '$start_date' AND '$end_date')";
        }
        
        if($status == 1){
            $kondisi_status = "AND a.invoice is NOT NULL";
        }else{
            $kondisi_status = '';
        }
        $data = $this->db->query("SELECT 
                                    a.invoice, a.dp, a.book_date as tanggal_transaksi, a.name_service as id_service, a.id_price, a.price, b.margin, c.name_service as nama_service, d.name as nama_pelanggan
                                FROM 
                                    booking_temp a
                                LEFT JOIN service_margin_price b ON a.name_service = b.service_id AND a.company_id = b.company_id
                                LEFT JOIN mst_service c ON a.name_service = c.id_service
                                LEFT JOIN mst_users d ON a.id_user = d.id_user
                                WHERE 
                                        a.company_id = $company_id AND a.statusDp = 1
                                            $kondisi $kondisi_status
                                    AND
                                        a.deleted_at is null
                                ORDER BY a.book_date ASC");
        return $data->result();
    }
    
    public function getAllDpTransaksiByIdCompany($company_id, $start_date,$end_date){
        if($start_date == '' && $end_date == ''){
            $kondisi = '';
        }
        if($start_date != '' && $end_date == ''){
            $kondisi = "AND DATE(a.book_date) >= '$start_date'";
        }
        if($start_date == '' && $end_date != ''){
            $kondisi = "AND DATE(a.book_date) <= '$end_date'";
        }
        if($start_date != '' && $end_date != ''){
            $kondisi = "AND (DATE(a.book_date) BETWEEN '$start_date' AND '$end_date')";
        }
        $data = $this->db->query("SELECT 
                                    a.invoice, a.dp, a.book_date as tanggal_transaksi, a.name_service as id_service, a.id_price, a.price, b.margin, c.name_service as nama_service, d.name as nama_pelanggan
                                FROM 
                                    booking_temp a
                                LEFT JOIN service_margin_price b ON a.name_service = b.service_id AND a.company_id = b.company_id
                                LEFT JOIN mst_service c ON a.name_service = c.id_service
                                LEFT JOIN mst_users d ON a.id_user = d.id_user
                                WHERE 
                                        a.company_id = $company_id AND a.statusDp = 1
                                            $kondisi
                                    AND
                                        a.deleted_at is null
                                ORDER BY a.book_date ASC");
        return $data->result();
    }
}
