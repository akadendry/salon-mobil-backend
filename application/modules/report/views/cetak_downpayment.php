<?php
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">
    <thead>
        <tr>
            <th>Nama Customer</th>
            <th>Nama Service</th>
            <th>Tanggal Service</th>
            <th>DP</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total_dp = 0;
        ?>
        <?php if (count($transaksi > 0)): ?>
            <?php foreach ($transaksi as $row): ?>
                <tr>
                    <td> <?= $row->nama_pelanggan ?></td>
                    <td> <?= $row->nama_service ?></td>
                    <td> <?= date('d-m-Y', strtotime($row->tanggal_transaksi)) ?></td>
                    <td> <b>Rp. <?= number_format($row->dp) ?></b></td>
                </tr>
                <?php $total_dp += $row->dp; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"><b>Total DP Yang Sudah Dibayarkan</b></td>
            <td colspan="1"><b> Rp. <?= number_format($total_dp) ?></b></td>
        </tr>
    </tfoot>
</table>