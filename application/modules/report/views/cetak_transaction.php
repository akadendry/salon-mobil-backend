<?php
header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");
?>

<table border="1" width="100%">

    <thead style="text-align: left;">
        <tr>
            <th>Nama Customer</th>
            <th>Nama Service</th>
            <th>Tanggal Service</th>
            <th>Status</th>
            <th>Harga</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 1;
        $total_hargas = 0;
        $total_price = 0;
        ?>
        <?php if (count($transaksi > 0)): ?>
            <?php foreach ($transaksi as $row): ?>
                <tr>
                    <td> <?= $row->nama_pelanggan ?></td>
                    <td> <?= $row->nama_service ?></td>
                    <td> <?= date('d-m-Y', strtotime($row->tanggal_transaksi)) ?></td>
                    <?php if ($row->invoice == null || $row->invoice == ''): ?>
                        <td>Belum Lunas</td>
                    <?php else: ?>
                        <td>Lunas</td>
                    <?php endif; ?>
                    <td><b> Rp. <?= number_format($row->price) ?></b></td>
                    <?php
                    $sql = "SELECT * FROM mst_price WHERE id_price = " . $row->id_price;
                    $query = $this->db->query($sql);
                    if ($query->num_rows() > 0) {
                        foreach ($query->result() as $rows) {
                            $harga_basic = $rows->price;
                        }
                    } else {
                        $harga_basic = 0;
                    }
                    $total_harga = (int) $row->price - (int) $harga_basic;
                    ?>
                </tr>
                <?php $total_hargas += $total_harga; ?>
                <?php $total_price += $row->price; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4"><b>Total Pendapatan</b></td>
            <td colspan="1"><b> Rp. <?= number_format($total_price) ?></b></td>
        </tr>
    </tfoot>

</table>