<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Status extends CI_Controller {



    function __construct() {

        parent::__construct();

        $this->load->database();

        $this->load->model('M_Status');

        $this->load->model('M_Precheck');

        $this->load->model('M_Datamaster');

        if ($this->session->userdata('email') == NULL) {

            return redirect('login');

        }

        date_default_timezone_set("Asia/Jakarta");

    }



    public function proses() {

        $company_id = $this->session->userdata('company_id');

        $this->data['dataProses'] = $this->M_Status->get_all_data_status_proses($company_id);

//        $jumlah = count($this->data['dataProses']);

//        for($i=0; $i<$jumlah; $i++){

//            $name_par = 'bay'.$i;

//            $this->data[$name_par] = $this->M_Datamaster->get_all_bay();

//        }

//        $this->data['bay0'] = $this->M_Datamaster->get_all_bay();

//        $this->data['bay1'] = $this->M_Datamaster->get_all_bay();

        $this->data['menuActiveParent'] = 'status';
        $this->data['menuActive'] = 'status_proses';

        $this->load->view('proses', $this->data);

    }



    public function ubahTempatPengerjaan() {

        $id_precheck = $this->input->post('id_precheck');

        $data = array(

            'tempat_pengerjaan' => $this->input->post('tempat_pengerjaan')

        );

        $updated = $this->M_Status->updatedTempatPengerjaan($data, $id_precheck);

        if ($updated == 1) {

            $this->session->set_flashdata('status', 'success');

            $this->session->set_flashdata('message', 'Tempat Pengerjaan berhasil Diubah');

            return redirect('status/proses');

        } else {

            $this->session->set_flashdata('status', 'danger');

            $this->session->set_flashdata('message', 'Terjadi kesalahan');

            return redirect('status/proses');

        }

    }



    public function prosesSelesai() {

        $id_precheck = $this->uri->segment(3);

        $id_booking = $this->uri->segment(4);

        $dataPrecheck = array(

            'status' => 2

        );

        $updated = $this->M_Status->updatedTempatPengerjaan($dataPrecheck, $id_precheck);

        if ($updated == 1) {

            $this->session->set_flashdata('status', 'success');

            $this->session->set_flashdata('message', 'Data pekerjaan berhasil diselesaikan');

            return redirect('status/proses');

        } else {

            $this->session->set_flashdata('status', 'danger');

            $this->session->set_flashdata('message', 'Terjadi kesalahan');

            return redirect('status/proses');

        }

    }



    public function selesai() {

        $company_id = $this->session->userdata('company_id');

        $this->data['dataSelesai'] = $this->M_Status->get_all_data_status_selesai($company_id);

        $this->data['menuActiveParent'] = 'status';
        $this->data['menuActive'] = 'status_selesai';

        $this->load->view('selesai', $this->data);

    }



    public function prosesDetail() {

        $company_id = $this->session->userdata('company_id');

        $this->data['menuActiveParent'] = 'status';
        $this->data['menuActive'] = 'status_selesai';

        $id_precheck = $this->uri->segment(3);

        $this->data['data_precheck'] = $this->M_Status->get_data_precheck_by_id($id_precheck, $company_id);

        $this->data['action'] = 'detail';

        $this->load->view('proses_detail', $this->data);

    }



    public function cetakInvoice() {

        $id_precheck = $this->input->post('id_precheck');

        $this->data['id_precheck'] = $id_precheck;

        $id_booking = $this->input->post('id_booking');

        $this->data['id_booking'] = $id_booking;

        $this->data['menuActiveParent'] = 'status';
        $this->data['menuActive'] = 'status_selesai';

        $id_size = $this->input->post('id_size');

        $id_service = $this->input->post('id_service');

        $this->data['nama_customer'] = $this->input->post('nama_customer');

        $this->data['no_polisi'] = $this->input->post('no_polisi');

        $this->data['merk'] = $this->input->post('merk');

        $this->data['type'] = $this->input->post('type');

        $this->data['ukuran_mobil'] = $this->input->post('ukuran_mobil');

        $this->data['jenis_pekerjaan'] = $this->input->post('jenis_pekerjaan');

        $diskon = $this->input->post('diskon');

        $this->data['diskon'] = $diskon;

        $price = str_replace('Rp. ', '', $this->input->post('price'));

        $price = str_replace(',', '', $price);

        $this->data['price'] = $price;

        $this->data['price_diskon'] = $price * $diskon / 100;

        $dp = str_replace('Rp. ', '', $this->input->post('dp'));

        $dp = str_replace(',', '', $dp);

        $this->data['total_bayar'] = ((int) $price - (int) $dp) - $this->data['price_diskon'];

        $email_customer = 'dendryjava@gmail.com';

//        $email_customer = $this->input->post('email_customer');

        $this->data['email_customer'] = $email_customer;

        if ($diskon > 0) {

            $hargaSetelahDiskon = $price - ($price * $diskon / 100);

        } else {

            $hargaSetelahDiskon = $price;

        }

        $this->data['hargaSetelahDiskon'] = $hargaSetelahDiskon;

        $this->data['action'] = 'belum';



        $this->data['dp'] = $dp;

        $dataDiskon = array(

            'diskon' => $diskon

        );

        $updateDiskon = $this->M_Precheck->updatedBooking($dataDiskon, $id_booking);

        if ($updateDiskon == 1) {

            //cetak dan save pdfnya

            $filenameinvoice = strtotime(date('Y-m-d H:i:s')) . ".pdf";

            $this->cetakInovicePdf($id_booking, $filenameinvoice);

            //akhir cetak dan save pdfnya

//            $config = Array(

//                'smtp_host' => 'mail.detailingshopgarage.com',

//                'smtp_port' => 587,

//                'smtp_user' => 'no-reply@detailingshopgarage.com', // change it to yours

//                'smtp_auth' => TRUE,

//                'smtp_pass' => 'Password1', // change it to yours

//                'mailtype' => 'html',

//                'charset' => 'utf-8',

//                'newline' => "\r\n",

//                'wordwrap' => TRUE

//            );

//            $pdfFilePath = FCPATH . "assets/file/invoice/" . $filenameinvoice;

//            $this->load->library('email', $config);

//            $this->email->set_newline("\r\n");

//            $this->email->from('admin@dsgindonesia.com', 'DSG Indonesia');

//            $this->email->to($email_customer); // change it to yours

//            $this->email->subject('Invoice Shop Garage');

//            $this->data['recipient_email'] = $email_customer;

//            $this->data['subjek_email'] = "Invoice";

//            $email = $this->load->view('v_email_template_invoice', $this->data, TRUE);

//            $this->email->message($email);

//            $this->email->attach($pdfFilePath);

//            $this->email->send();

//            if ($this->email->send()) {

//                echo 'berhasil';

//                exit();

//            } else {

//                show_error($this->email->print_debugger());

//                exit();

//            }

//            $this->load->view('cetakInvoice', $this->data);

            return redirect('status/prosesDetail/' . $id_precheck . '/' . $id_booking);

        } else {

            $this->session->set_flashdata('status', 'danger');

            $this->session->set_flashdata('message', 'Terjadi kesalahan, silahkan coba lagi!');

            return redirect('status/prosesDetail/' . $id_precheck . '/' . $id_booking);

        }

    }



    public function selesaiInvoice() {

        $company_id = $this->session->userdata('company_id');

        $this->data['menuActiveParent'] = 'status';
        $this->data['menuActive'] = 'status_selesai';

        $id_precheck = $this->uri->segment(3);

        $id_booking = $this->uri->segment(4);

        $this->data['id_precheck'] = $id_precheck;

        $this->data['id_booking'] = $id_booking;

        $data_precheck = $this->M_Precheck->get_data_precheck_by_id($id_precheck, $company_id);

        foreach ($data_precheck as $row) {

            $this->data['nama_customer'] = $row->name;

            $this->data['no_polisi'] = $row->no_polisi;

            $this->data['merk'] = $row->merk_mobil;

            $this->data['type'] = $row->type_mobil;

            $this->data['ukuran_mobil'] = $row->name_size;

            $this->data['jenis_pekerjaan'] = $row->nama_servis;

            $this->data['dp'] = $row->dp;

            $diskon = $row->diskon;

            $this->data['diskon'] = $diskon;

            $price = str_replace('Rp. ', '', $row->price);

            $price = str_replace(',', '', $price);

            $this->data['price'] = $price;

            $this->data['price_diskon'] = $price * $diskon / 100;

            $this->data['total_bayar'] = ((int) $price - (int) $row->dp) - $this->data['price_diskon'];

            if ($diskon > 0) {

                $hargaSetelahDiskon = $price - ($price * $diskon / 100);

            } else {

                $hargaSetelahDiskon = $price;

            }

            $this->data['hargaSetelahDiskon'] = $hargaSetelahDiskon;

            $this->data['invoice'] = $row->invoice;

            $this->data['action'] = 'sudah';

        }

        $this->load->view('cetakInvoice', $this->data);

    }



    public function cetakInovicePdf($id_booking, $filename) {

        //get datanya bung

        $data['data'] = $this->M_Precheck->get_data_transaction_by_id_booking($id_booking);

//        foreach ($data['data'] as $row):

//            //base 64 image

//            $path = base_url('assets/images/precheck/' . str_replace(' ', '-', $row->no_polisi) . '/' . $row->sign_admin);

//            $type = pathinfo($path, PATHINFO_EXTENSION);

//            $datas = file_get_contents($path);

//            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($datas);

//            $data['data'][0]->sign_admin_64 = $base64;

//        endforeach;



        //cetak dan save pdfnya

        #--CREATE PDF

        $this->load->library('pdf');

        $data['tanggal'] = date('d M Y');

        $html = $this->load->view('pdf_invoice', $data, true);

        $this->pdf->createPDF($html, $filename);



        //update invoice di db

        $dataBooking = array(

            'invoice' => $filename

        );

        $this->M_Precheck->updatedBooking($dataBooking, $id_booking);

        //akhir update data

    }



}

