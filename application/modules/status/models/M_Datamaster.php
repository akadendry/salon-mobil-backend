<?php

class M_Datamaster extends CI_Model {
    
    public function getAllDataCustomer() {
        $data = $this->db->query("SELECT *
                                  FROM mst_users 
                                  ORDER BY id_user ASC");
        return $data->result();
    }
    
    public function getAllDataUserCms() {
        $data = $this->db->query("SELECT a.*, b.name as role_name
                                  FROM mst_users_cms a 
                                  LEFT JOIN roles b ON a.role = b.id_role 
                                  WHERE deleted_at is null
                                  ORDER BY a.id_user ASC");
        return $data->result();
    }
    
    public function getAllDataUserCmsByBranch($company_id) {
        $data = $this->db->query("SELECT a.*, b.name as role_name
                                  FROM mst_users_cms a 
                                  LEFT JOIN roles b ON a.role = b.id_role 
                                  WHERE a.deleted_at is null AND company_id = $company_id
                                  ORDER BY a.id_user ASC");
        return $data->result();
    }
    
    public function getAllDataRole() {
        $data = $this->db->query("SELECT *
                                  FROM roles
                                  WHERE id_role != 1
                                  ORDER BY id_role DESC");
        return $data->result();
    }
    
    public function getAllDataCompany() {
        $data = $this->db->query("SELECT *
                FROM mst_company
                WHERE deleted_at is null
                ORDER BY id ASC");
        return $data->result();
    }
    
    public function getAllDataBrand() {
        $data = $this->db->query("SELECT *
                                  FROM mst_brand 
                                  WHERE deleted_at is null
                                  ORDER BY name ASC");
        return $data->result();
    }
    
    public function getAllSize() {
        $data = $this->db->query("SELECT *
                                  FROM car_size 
                                  WHERE deleted_at is null
                                  ORDER BY id_size ASC");
        return $data->result();
    }
    
    public function getAllBayByCompany($company_id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_bay 
                                  WHERE deleted_at is null AND company_id = $company_id
                                  ORDER BY id DESC");
        return $data->result();
    }
    
    public function getAllRekeningByCompany($company_id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_rekening 
                                  WHERE deleted_at is null AND company_id = $company_id
                                  ORDER BY id DESC");
        return $data->result();
    }
    
    public function getAllBay() {
        $data = $this->db->query("SELECT *
                                  FROM mst_bay 
                                  WHERE deleted_at is null
                                  ORDER BY id DESC");
        return $data->result();
    }
    
    public function get_all_bay() {
        $data = $this->db->query("SELECT *
                                  FROM mst_bay 
                                  WHERE deleted_at is null
                                  ORDER BY id ASC");
        return $data->result();
    }
    
    public function getBrandById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_brand 
                                  WHERE deleted_at is null AND id = '$id'");
        return $data->result();
    }
    
    public function getMarginById($id) {
        $data = $this->db->query("SELECT *
                                  FROM service_margin_price 
                                  WHERE deleted_at is null AND id = '$id'");
        return $data->result();
    }
    
    public function getRekeningById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_rekening 
                                  WHERE deleted_at is null AND id = '$id'");
        return $data->result();
    }
    
    public function getSizeById($id) {
        $data = $this->db->query("SELECT *
                                  FROM car_size 
                                  WHERE deleted_at is null AND id_size = '$id'");
        return $data->result();
    }
    
    public function getBayById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_bay 
                                  WHERE deleted_at is null AND id = '$id'");
        return $data->result();
    }
    
    public function getUserCmsById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_users_cms 
                                  WHERE deleted_at is null AND id_user = '$id'");
        return $data->result();
    }
    
    public function getTypeById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_brand_type
                                  WHERE deleted_at is null AND id = '$id'");
        return $data->result();
    }
    
    public function getServiceById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_service
                                  WHERE deleted_at is null AND id_service = '$id'");
        return $data->result();
    }
    
    public function getPriceById($id) {
        $data = $this->db->query("SELECT *
                                  FROM mst_price
                                  WHERE deleted_at is null AND id_price = '$id'");
        return $data->result();
    }
    
    public function getAllDataType() {
        $data = $this->db->query("SELECT a.*, b.name as brand_name, c.name_size
                                  FROM mst_brand_type a
                                  JOIN mst_brand b ON a.brand_id = b.id
                                  JOIN car_size c ON a.size_id = c.id_size
                                  WHERE a.deleted_at is null
                                  ORDER BY a.name ASC");
        return $data->result();
    }
    
    public function getAllDataSize() {
        $data = $this->db->query("SELECT *
                                  FROM car_size  
                                  WHERE deleted_at is null
                                  ORDER BY name_size ASC");
        return $data->result();
    }
    
    public function getAllDataService() {
        $data = $this->db->query("SELECT * 
                                  FROM mst_service  
                                  WHERE is_active = 1 AND deleted_at is null
                                  ORDER BY id_service DESC");
        return $data->result();
    }
    
    public function getAllDataServiceMerge($company_id) {
        $data = $this->db->query("SELECT a.*, b.*
                                  FROM service_margin_price a
                                  LEFT JOIN mst_service b ON a.service_id = b.id_service
                                  WHERE b.is_active = 1 AND a.deleted_at is null AND a.company_id = $company_id
                                  ORDER BY b.id_service DESC");
        return $data->result();
    }
    
    public function getAllDataPrice() {
        $data = $this->db->query("SELECT a.*, b.name_service, c.name_size
                                  FROM mst_price a
                                  JOIN mst_service b ON a.id_service = b.id_service
                                  JOIN car_size c ON a.id_size = c.id_size
                                  WHERE a.deleted_at is null
                                  ORDER BY a.id_price DESC");
        return $data->result();
    }
    
    public function getAllDataPriceMargin($company_id) {
        $data = $this->db->query("SELECT a.*, b.name_service, c.name_size, (a.price * d.margin / 100) as price_total
                                  FROM mst_price a
                                  JOIN mst_service b ON a.id_service = b.id_service
                                  JOIN car_size c ON a.id_size = c.id_size
                                  RIGHT JOIN service_margin_price d ON b.id_service = d.service_id
                                  WHERE d.deleted_at is null AND d.company_id = $company_id
                                  ORDER BY a.id_price DESC");
        return $data->result();
    }
    
    public function getTypeCarByBrand($brand_id) {
        $data = $this->db->query("SELECT a.*, b.name as brand_name, c.name_size
                                  FROM mst_brand_type a
                                  JOIN mst_brand b ON a.brand_id = b.id
                                  JOIN car_size c ON a.size_id = c.id_size
                                  WHERE a.brand_id = '$brand_id'
                                  ORDER BY a.name ASC");
        return $data->result();
    }
    
    public function getDataCompanyById($company_id) {
        $data = $this->db->query("SELECT * FROM mst_company WHERE id = $company_id AND deleted_at is null");
        return $data->result();
    }
    
    public function getUserCmsByCompany($company_id) {
        $data = $this->db->query("SELECT a.*, b.name as role_name
                                  FROM mst_users_cms a 
                                  LEFT JOIN roles b ON a.role = b.id_role 
                                  WHERE deleted_at is null AND a.company_id = $company_id
                                  ORDER BY a.id_user ASC");
        return $data->result();
    }
    
    public function getMarginByCompany($company_id) {
        $data = $this->db->query("SELECT a.*, b.name_service
                                  FROM service_margin_price a 
                                  RIGHT JOIN mst_service b ON a.service_id = b.id_service 
                                  WHERE a.deleted_at is null AND a.company_id = $company_id
                                  ORDER BY a.id DESC");
        return $data->result();
    }
    
    public function getSizeByType($type_id) {
        $data = $this->db->query("SELECT * FROM mst_brand_type WHERE id = $type_id AND deleted_at is null");
        return $data->result();
    }
    
    public function getPrice($service_id, $size_id) {
        $data = $this->db->query("SELECT * FROM mst_price WHERE id_service = $service_id AND id_size = '$size_id' AND deleted_at is null");
        return $data->result();
    }
    
    public function insertCompany($data_insert) {
        $this->db->insert('mst_company', $data_insert);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    public function insertMargin($data) {
        $query = $this->db->insert('service_margin_price', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertRekening($data_insert) {
        $query = $this->db->insert('mst_rekening', $data_insert);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertBrand($data){
        $query = $this->db->insert('mst_brand', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertSize($data){
        $query = $this->db->insert('car_size', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertBay($data){
        $query = $this->db->insert('mst_bay', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertUserCms($data){
        $query = $this->db->insert('mst_users_cms', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertType($data){
        $query = $this->db->insert('mst_brand_type', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertService($data){
        $query = $this->db->insert('mst_service', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function insertPrice($data){
        $query = $this->db->insert('mst_price', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateBrand($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('mst_rekening', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateMargin($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('service_margin_price', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateRekening($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('mst_rekening', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateCompany($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('mst_company', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateSize($data, $id){
        $this->db->where('id_size', $id);
        $query = $this->db->update('car_size', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateBay($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('mst_bay', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateUser($data, $id){
        $this->db->where('id_user', $id);
        $query = $this->db->update('mst_users_cms', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateType($data, $id){
        $this->db->where('id', $id);
        $query = $this->db->update('mst_brand_type', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateService($data, $id){
        $this->db->where('id_service', $id);
        $query = $this->db->update('mst_service', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updatePrice($data, $id){
        $this->db->where('id_price', $id);
        $query = $this->db->update('mst_price', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
}