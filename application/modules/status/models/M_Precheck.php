<?php

class M_Precheck extends CI_Model {

    public function get_list_precheck($company_id) {
        $data = $this->db->query("SELECT a.*, b.book_date, b.status_inspect, e.name as brand_name, f.name as car_type_name , c.name as nama_customer, c.email, c.phone, d.name_service
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                LEFT JOIN mst_service d ON b.name_service = d.id_service
                LEFT JOIN mst_brand e ON b.car_brand = e.id
                LEFT JOIN mst_brand_type f ON b.car_type = f.id
		WHERE b.status_bayar = 1 AND a.status = 0  AND DATE(b.book_date) = CURDATE() AND b.company_id = $company_id");
        return $data->result();
    }

    public function get_list_all_data_precheck($company_id) {
        $data = $this->db->query("SELECT DISTINCT a.*, d.name_service as nama_servis, b.car_brand, b.car_type, b.name_service, b.car_color, b.book_date, c.name as nama_customer, c.email, c.phone, e.name as brand_name, f.name as car_type_name 
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                LEFT JOIN mst_service d ON b.name_service = d.id_service
                LEFT JOIN mst_brand e ON b.car_brand = e.id
                LEFT JOIN mst_brand_type f ON b.car_type = f.id
		WHERE b.status_bayar = 1 AND b.company_id = $company_id ORDER BY a.created_at DESC");
//		WHERE b.status_bayar = 1 ORDER BY b.book_date, a.status ASC");
        return $data->result();
    }
    
    public function get_list_all_data_precheck_byDatePrecheck($precheckDate, $status, $company_id) {
        if($precheckDate == ''){
            $kondisiPrecheckDate = '';
        }else{
            $kondisiPrecheckDate = "AND b.book_date = '$precheckDate'";
        }
        if($status == ''){
            $kondisiStatus = '';
        }elseif($status == 3){
            $kondisiStatus = "AND a.status = 0 AND DATE(b.book_date) > CURDATE() ";
        }else{
            $kondisiStatus = "AND a.status = '$status'";
        }
        $data = $this->db->query("SELECT a.*, d.name_service as nama_servis, b.car_brand, b.car_type, b.name_service, b.car_color, b.book_date, c.name as nama_customer, c.email, c.phone
		FROM precheck a 
		LEFT JOIN booking_temp b ON a.id_booking = b.id_booking
		LEFT JOIN mst_users c ON b.id_user = c.id_user
                JOIN mst_service d ON b.name_service = d.id_service
		WHERE b.company_id = $company_id AND b.status_bayar = 1 $kondisiPrecheckDate $kondisiStatus");
//        echo $this->db->last_query();
//        exit();
        return $data->result();
    }

    public function get_list_precheck_not_approvment() {
        $data = $this->db->query("SELECT a.*, b.name, b.email, b.phone FROM precheck a LEFT JOIN mst_users b ON a.id_users = b.id_user WHERE a.status = 0");
        return $data->result();
    }

    public function get_data_precheck_by_id($id_precheck, $company_id) {
        $data = $this->db->query("SELECT a.*, d.name_service as nama_servis, b.name, b.email, c.*, c.name_service as service_id, d.name_service, e.name_size, f.name as merk_mobil, g.name as type_mobil
                FROM precheck a 
                LEFT JOIN booking_temp c ON a.id_booking = c.id_booking 
                LEFT JOIN mst_users b ON c.id_user = b.id_user 
                LEFT JOIN mst_service d ON c.name_service = d.id_service
                LEFT JOIN car_size e ON c.id_size = e.id_size
                LEFT JOIN mst_brand f ON f.id = c.car_brand
                LEFT JOIN mst_brand_type g ON g.id = c.car_type
                WHERE a.id = $id_precheck AND c.company_id = $company_id");
        return $data->result();
    }

    public function getIdBookingByIdPrecheck($id_precheck) {
        $data = $this->db->query("SELECT id_booking FROM precheck WHERE id = $id_precheck");
        $rsl = $data->result();
        $idBooking = '';
        foreach ($rsl as $row) {
            $idBooking = $row->id_booking;
        }
        return $idBooking;
    }

    public function get_list_users() {
        $data = $this->db->query("SELECT * FROM mst_users WHERE verified = 1");
        return $data->result();
    }

    public function insert($data_insert) {
        $query = $this->db->insert('precheck', $data_insert);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function insertBooking($data_insert) {
        $this->db->insert('booking_temp', $data_insert);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function updated($data, $id_precheck) {
        $this->db->where('id', $id_precheck);
        $query = $this->db->update('precheck', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updatePrecheckByIdbooking($data, $idBooking) {
        $this->db->where('id_booking', $idBooking);
        $query = $this->db->update('precheck', $data);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updatedBooking($dataBooking, $idBooking) {
        $this->db->where('id_booking', $idBooking);
        $query = $this->db->update('booking_temp', $dataBooking);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function get_data_transaction_by_id_booking($id_booking){
        $data = $this->db->query("SELECT a.*,
                b.name_service as nama_servis, c.name as nama_brand, 
                d.name as nama_tipe, e.name_size as ukuran, f.name as nama_customer, g.sign_admin
                FROM  booking_temp a 
                LEFT JOIN mst_service b ON a.name_service = b.id_service
                LEFT JOIN mst_brand c ON a.car_brand = c.id
                LEFT JOIN mst_brand_type d ON a.car_type = d.id
                LEFT JOIN car_size e ON a.id_size = e.id_size
                LEFT JOIN mst_users f ON a.id_user = f.id_user
                LEFT JOIN precheck g ON a.id_booking = g.id_booking
                WHERE a.id_booking = $id_booking");
        return $data->result();
    }

}
