<?php

class M_Status extends CI_Model
{

	function get_all_data_status_proses($company_id)
	{
		$data = $this->db->query("SELECT g.name as nama_bay, a.*, b.email, b.name, b.phone, c.*, d.name_service as nama_servis, e.name as nama_brand, f.name as nama_tipe 
		FROM booking_temp a 
		JOIN mst_users b ON a.id_user = b.id_user
		JOIN precheck c ON a.id_booking = c.id_booking
                JOIN mst_service d ON a.name_service = d.id_service
                JOIN mst_brand e ON a.car_brand = e.id
                JOIN mst_brand_type f ON a.car_type= f.id
                LEFT JOIN mst_bay g ON g.id = c.tempat_pengerjaan
		WHERE a.status_bayar = 1 AND c.status = 1 AND a.company_id = $company_id");
		return $data->result();
	}

	function get_all_data_status_selesai($company_id)
	{
		$data = $this->db->query("SELECT g.name as nama_bay, a.*, b.email, b.name, b.phone, c.*, d.name_service as nama_servis, e.name as nama_merk, f.name as nama_tipe
		FROM booking_temp a 
		JOIN mst_users b ON a.id_user = b.id_user
		JOIN precheck c ON a.id_booking = c.id_booking
                LEFT JOIN mst_service d ON a.name_service = d.id_service
                LEFT JOIN mst_brand e ON e.id = a.car_brand
                LEFT JOIN mst_brand_type f ON f.id = a.car_type
                LEFT JOIN mst_bay g ON g.id = c.tempat_pengerjaan
		WHERE a.status_bayar = 1 AND c.status = 2 AND a.company_id = $company_id");
		return $data->result();
	}

	public function updatedTempatPengerjaan($data, $id_precheck)
	{
		$this->db->where('id', $id_precheck);
		$query = $this->db->update('precheck', $data);
		if ($query) {
			return 1;
		} else {
			return 0;
		}
	}
        
        public function get_data_precheck_by_id($id_precheck, $company_id) {
        $data = $this->db->query("SELECT a.*, d.name_service as nama_servis, b.name, b.email, c.*, c.name_service as service_id, d.name_service, e.name_size, f.name as merk_mobil, g.name as type_mobil
                FROM precheck a 
                LEFT JOIN booking_temp c ON a.id_booking = c.id_booking 
                LEFT JOIN mst_users b ON c.id_user = b.id_user 
                LEFT JOIN mst_service d ON c.name_service = d.id_service
                LEFT JOIN car_size e ON c.id_size = e.id_size
                LEFT JOIN mst_brand f ON f.id = c.car_brand
                LEFT JOIN mst_brand_type g ON g.id = c.car_type
                WHERE a.id = $id_precheck AND c.company_id = $company_id");
        return $data->result();
    }
}
