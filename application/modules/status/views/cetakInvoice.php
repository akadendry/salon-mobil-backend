<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title>Precheck</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery-ui.custom.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css') ?>" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css') ?>" />
        <link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,600,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:100,300,400,500,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css') ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css') ?>" />


        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js') ?>"></script>

        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <style>
            select {
                -webkit-appearance: none;
                /*webkit browsers */
                -moz-appearance: none;
                /*Firefox */
                appearance: none;
                /* modern browsers */
                border-radius: 0;

            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            #imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            /*#imageTampak1, #imageTampak2, #imageTampak3, #imageTampak4 :hover {opacity: 0.7;}*/

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            .modal-content-inovoice {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: 80%;
            }

            /* Caption of Modal Image */
            #caption, #caption2, #caption3, #caption4, #caption5 {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation */
            .modal-content, #caption, #caption2, #caption3, #caption4, #caption5 {  
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close, .close2, .close3, .close4, .close5 {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close, .close2, .close3, .close4, .close5:hover,
            .close, .close2, .close3, .close4, .close5:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }
        </style>
        <script>
            window.setTimeout(function () {
                $(".alert").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        </script>
    </head>

    <body class="no-skin">
        <div id="navbar" class="navbar navbar-default  ace-save-state">
            <div class="navbar-container ace-save-state" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="index.html" class="navbar-brand">
                        <small>
                            <i class="fa fa-car"></i>
                            DSG Dashboard
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="light-blue dropdown-modal">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                    <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
                                <span class="user-info">
                                    <small>Welcome,</small>
                                    <?= $this->session->userdata("name") ?>
                                </span>

                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?= base_url('login/changePassword') ?>">
                                        <i class="ace-icon fa fa-lock"></i>
                                        Change Password
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= base_url('login/process_logout') ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.navbar-container -->
        </div>

        <?php $this->load->view('v_sideMenu'); ?>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            Invoice
                        </h1>
                    </div><!-- /.page-header -->
                    <?php if ($this->session->flashdata('message')): ?>
                        <div class="alert alert-<?= $this->session->flashdata('status'); ?>">
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="widget-box">
                                <div class="widget-header widget-header-blue widget-header-flat">
                                    <h4 class="widget-title lighter">Data Inovice</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div id="fuelux-wizard-container">
                                            <div class="step-content pos-rel">
                                                <form method="post" class="form-horizontal" id="sample-form">
<!--                                                    <div class="form-group">
                                                        <div style="float: right; padding-right: 10px;">
                                                            <a href="</?php echo base_url('status/cetakInovicePdf/'.$id_precheck); ?>" class="btn btn-danger">
                                                                <i class="fa fa-print">
                                                                    PDF
                                                                </i>
                                                            </a>
                                                            <a href="</?php echo base_url('status/cetakInoviceExcel/'.$id_precheck); ?>" class="btn btn-success">
                                                                <i class="fa fa-print">
                                                                    Excel
                                                                </i>
                                                            </a>
                                                        </div>
                                                    </div>-->
                                                    <br>
                                                    <div style="overflow-x:auto;">
                                                        <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nama Customer</th>
                                                                    <th>No Polisi</th>
                                                                    <th>Mobil</th>
                                                                    <th>Ukuran Mobil</th>
                                                                    <th>Jenis Service</th>
                                                                    <th>Harga</th>
                                                                    <th>DP</th>
                                                                    <th>Diskon</th>
                                                                    <th>Sub Total</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                <tr>
                                                                    <td><?= strtoupper($nama_customer) ?></td>
                                                                    <td><?= strtoupper($no_polisi); ?></td>
                                                                    <td><?= strtoupper($merk . ' ' . $type); ?></td>
                                                                    <td><?= strtoupper($ukuran_mobil); ?></td>
                                                                    <td>
                                                                        <?= strtoupper($jenis_pekerjaan); ?>
                                                                    </td>
                                                                    <td>
                                                                        Rp. <?= number_format($price); ?>
                                                                    </td>
                                                                    <td>
                                                                        Rp. <?= number_format($dp) ?>
                                                                    </td>
                                                                    <?php if ($diskon == 0): ?>
                                                                        <td> - </td>
                                                                    <?php else: ?>
                                                                        <td><?= $diskon . '% (Rp. '. number_format($price_diskon).')'; ?></td>
                                                                    <?php endif; ?>
                                                                    <td>Rp. <?= number_format($total_bayar); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="8" style="text-align: right;"><b>Sub Total Harga </b></td>
                                                                    <td> <b>Rp. <?= number_format($total_bayar); ?></b> </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div style="display: none;">
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right" >Nama Customer : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= strtoupper($nama_customer) ?></label>
                                                            <input type="hidden" name="nama_customer" value="<?= $nama_customer ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">No Polisi : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= strtoupper($no_polisi); ?></label>
                                                            <input type="hidden" name="no_polisi" value="<?= $no_polisi; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Mobil : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= strtoupper($merk . ' ' . $type); ?></label>
                                                            <input type="hidden" name="merk" value="<?= $merk; ?>"/>
                                                            <input type="hidden" name="type" value="<?= $type; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Ukuran Mobil : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= strtoupper($ukuran_mobil); ?></label>
                                                            <input type="hidden" name="ukuran_mobil" value="<?= $ukuran_mobil; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Jenis Service : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= strtoupper($jenis_pekerjaan); ?></label>
                                                            <input type="hidden" name="jenis_pekerjaan" value="<?= $jenis_pekerjaan; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Harga : </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;">Rp. <?= number_format($price); ?></label>
                                                            <input type="hidden" name="price" value="<?= $price; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Diskon : </label>
                                                            <?php if ($diskon == 0): ?>
                                                                <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"> - </label>
                                                            <?php else: ?>
                                                                <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;"><?= $diskon . '%'; ?></label>
                                                            <?php endif; ?>
                                                            <input type="hidden" name="diskon" value="<?= $diskon; ?>"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-12 col-sm-3 control-label no-padding-right">Sub Total: </label>
                                                            <label class="col-xs-3 col-sm-6 control-label no-padding-right" style="text-align:left;">Rp. <?= number_format($hargaSetelahDiskon); ?></label>
                                                            <input type="hidden" name="hargaSetelahDiskon" value="<?= $hargaSetelahDiskon; ?>"/>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <hr />
                                        <form action="<?php echo base_url('status/prosesDetail/' . $id_precheck . '/' . $id_booking); ?>" method="post">
                                            <div class="wizard-actions">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Back
                                                </button>
                                                <?php if($action == 'sudah'):?>
                                                <a href="<?= base_url('assets/file/invoice/'.$invoice) ?>" class="btn btn-success" target="_blank">
                                                    <i class="ace-icon fa fa-print"></i>
                                                    Cetak Invoice
                                                </a>
                                                <?php endif;?>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
            </div><!-- /.main-content -->


            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->


        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.buttons.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.flash.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.html5.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.print.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/buttons.colVis.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/dataTables.select.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/sign/numeric-1.2.6.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/sign/bezier.js') ?>"></script>
        <script src="<?= base_url('assets/js/sign/jquery.signaturepad.js') ?>"></script>

        <script type='text/javascript' src="https://github.com/niklasvh/html2canvas/releases/download/0.4.1/html2canvas.js"></script>
        <script src="./js/json2.min.js"></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?>"></script>
    </body>

</html>
