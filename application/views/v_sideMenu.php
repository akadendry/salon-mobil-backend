<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>
        <ul class="nav nav-list">
            <!-- Kodingan baru yang udah dinamis -->
            <?php foreach($this->session->userdata("menu") as $key=>$value ): ?>
                <?php if(!array_key_exists("child",$value)):?>
                    <!-- Ini kalo menunya ga punya child -->
                    <?php if ($menuActive == $value['slug']) : ?>
                        <li class="active">
                    <?php else:?>
                        <li>
                    <?php endif;?>
                    <a href="<?= base_url($value['url']) ?>">
                        <i class="menu-icon <?= $value['icon'] ?>"></i>
                        <span class="menu-text"> <?= $value['name'] ?> </span>
                    </a>
                    <b class="arrow"></b>
                    </li>
                <?php else : ?>
                    <!-- Ini menunya kalo punya child -->
                    <?php if ($menuActiveParent == $value['slug']) : ?>
                    <li class="active open">
                    <?php else:?>
                    <li>
                    <?php endif;?>
                        <a href="#" class="dropdown-toggle">
                            <i class="menu-icon fa <?= $value['icon'] ?>"></i>
                            <span class="menu-text"> <?= $value['name'] ?> </span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>
                        <ul class="submenu">
                            <?php foreach($value['child'] as $rows): ?>
                                <?php if ($menuActive == 'transactionReport') : ?>
                                <li class="active">
                                <?php else : ?>
                                <li>
                                <?php endif;?>
                                <a href="<?= base_url($rows->url) ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    <?= $rows->name ?>
                                </a>
                                <b class="arrow"></b>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                <?php endif;?>
            <?php endforeach;?>
            <!-- Batas akhir kodingan baru-->
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
