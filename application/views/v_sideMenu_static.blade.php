<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>
        <ul class="nav nav-list">
            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                <?php if ($menuActive == 'booking') : ?>
                    <li class="active">
                    <?php else : ?>
                    <li>
                    <?php endif; ?>
                    <a href="<?= base_url('booking') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text"> Booking </span>
                    </a>
                    <b class="arrow"></b>
                </li>
            <?php endif; ?>
            <?php if ($menuActive == 'precheck') : ?>
                <li class="active open">
                <?php else : ?>
                <li>
                <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Pre Check </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <?php if ($menuActive == 'precheck') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('precheck') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Pre Check Inspection
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            <?php if ($menuActive == 'proses' || $menuActive == 'selesai') : ?>
                <li class="active open">
                <?php else : ?>
                <li>
                <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text"> Status </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <?php if ($menuActive == 'proses') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('status/proses') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Proses
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <?php if ($menuActive == 'selesai') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('status/selesai') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Selesai
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                <?php if ($menuActive == 'dataPrecheck') : ?>
                    <li class="active">
                    <?php else : ?>
                    <li>
                    <?php endif; ?>
                    <a href="<?= base_url('precheck/dataPrecheck') ?>">
                        <i class="menu-icon fa fa-database"></i>
                        <span class="menu-text"> Data Precheck </span>
                    </a>
                    <b class="arrow"></b>
                </li>
            <?php endif; ?>
            <?php if ($this->session->userdata("role") == 1 || $this->session->userdata("role") == 3) : ?>
                <?php if ($menuActive == 'user' || $menuActive == 'rekening' || $menuActive == 'rekening' || $menuActive == 'company' || $menuActive == 'customer' || $menuActive == 'brand' || $menuActive == 'type' || $menuActive == 'service' || $menuActive == 'price' || $menuActive == 'bay' || $menuActive == 'size') : ?>
                    <li class="active open">
                    <?php else : ?>
                    <li>
                    <?php endif; ?>
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-list"></i>
                        <span class="menu-text"> Data Masater </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <?php if ($this->session->userdata('company_id') == 1): ?>
                            <?php if ($menuActive == 'company') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/company') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Branch
                                </a>
                                <b class="arrow"></b>
                            </li>
                        <?php endif; ?>
                        <?php if ($menuActive == 'user') : ?>
                            <li class="active">
                            <?php else: ?>
                            <li class="">
                            <?php endif; ?>
                            <a href="<?= base_url('datamaster/user') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                User
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <?php if ($menuActive == 'customer') : ?>
                            <li class="active">
                            <?php else: ?>
                            <li class="">
                            <?php endif; ?>
                            <a href="<?= base_url('datamaster/customer') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Customer
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <?php if ($this->session->userdata('company_id') == 1): ?>
                            <?php if ($menuActive == 'brand') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/brand') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Merek Mobil
                                </a>
                                <b class="arrow"></b>
                            </li>
                            <?php if ($menuActive == 'type') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/type') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Tipe Mobile
                                </a>
                                <b class="arrow"></b>
                            </li>
                        <?php endif;?>
                            <?php if ($menuActive == 'service') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/service') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Service
                                </a>
                                <b class="arrow"></b>
                            </li>
                            <?php if ($menuActive == 'price') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/price') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Harga
                                </a>
                                <b class="arrow"></b>
                            </li>
                        <?php if ($menuActive == 'bay') : ?>
                            <li class="active">
                        <?php else: ?>
                            <li class="">
                        <?php endif; ?>
                            <a href="<?= base_url('datamaster/bay') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Bay
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <?php if ($this->session->userdata('company_id') == 1): ?>
                            <?php if ($menuActive == 'size') : ?>
                                <li class="active">
                                <?php else: ?>
                                <li class="">
                                <?php endif; ?>
                                <a href="<?= base_url('datamaster/size') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Size
                                </a>
                                <b class="arrow"></b>
                            </li>
                        <?php endif; ?>
                        <?php if ($menuActive == 'rekening') : ?>
                            <li class="active">
                        <?php else: ?>
                            <li class="">
                        <?php endif; ?>
                            <a href="<?= base_url('datamaster/rekening') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Rekening
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
                
                <?php if ($this->session->userdata('company_id') == 1): ?>
                <?php if ($menuActive == 'branchTransaction' || $menuActive == 'favoriteService') : ?>
                <li class="active open">
                <?php else : ?>
                <li>
                <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-building"></i>
                    <span class="menu-text"> Branch </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <?php if ($menuActive == 'branchTransaction') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('branch/transaction') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Transaksi
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <!--?php if ($menuActive == 'favoriteService') : ?>
                        <li class="active">
                        </?php else : ?>
                        <li>
                        </?php endif; ?>
                        <a href="</?= base_url('branch/favorite') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Service Terfavorit
                        </a>
                        <b class="arrow"></b>
                    </li-->
                </ul>
            </li>
            <?php endif;?>
            <?php if ($menuActive == 'transactionReport' || $menuActive == 'dpReport') : ?>
                <li class="active open">
                <?php else : ?>
                <li>
                <?php endif; ?>
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-book"></i>
                    <span class="menu-text"> Report </span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <?php if ($menuActive == 'transactionReport') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('report/transcation') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Transaksi
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <?php if ($menuActive == 'dpReport') : ?>
                        <li class="active">
                        <?php else : ?>
                        <li>
                        <?php endif; ?>
                        <a href="<?= base_url('report/dp') ?>">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Down Payment
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
